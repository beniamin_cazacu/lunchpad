package com.example.benni.lunchpad.Models;

/**
 * Created bye BENNI on 9/14/2016.
 * Keep all user information
 */
public class UserInformation {
    private String name;
    private String email;
    private String password;
    private String password_confirmation;
    private String phone;
    private String address;
    private String token;
    private String avatar;

    public UserInformation(String name, String email, String password, String password_confirmation, String phone, String address, String token, String avatar, String picturePath) {
        this.name = name;
        this.email = email;
        this.password = password;
        this.password_confirmation = password_confirmation;
        this.phone = phone;
        this.address = address;
        this.token = token;
        this.avatar = avatar;
    }
    public UserInformation(){

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword_confirmation() {
        return password_confirmation;
    }

    public void setPassword_confirmation(String password_confirmation) {
        this.password_confirmation = password_confirmation;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

}

