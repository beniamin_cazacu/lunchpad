package com.example.benni.lunchpad.Response.MenusList;

/**
 * Created bye BENNI on 9/30/2016.
 */

public class CodeMenuResponse {
    private Integer order_id;
    private String first_dish;
    private String second_dish;
    private String dessert;
    private String order_status;

    public CodeMenuResponse(){}

    public CodeMenuResponse(Integer order_id, String first_dish, String second_dish, String dessert, String order_status) {
        this.order_id = order_id;
        this.first_dish = first_dish;
        this.second_dish = second_dish;
        this.dessert = dessert;
        this.order_status = order_status;
    }

    public Integer getOrder_id() {
        return order_id;
    }

    public void setOrder_id(Integer order_id) {
        this.order_id = order_id;
    }

    public String getFirst_dish() {
        return first_dish;
    }

    public void setFirst_dish(String first_dish) {
        this.first_dish = first_dish;
    }

    public String getSecond_dish() {
        return second_dish;
    }

    public void setSecond_dish(String second_dish) {
        this.second_dish = second_dish;
    }

    public String getDessert() {
        return dessert;
    }

    public void setDessert(String dessert) {
        this.dessert = dessert;
    }

    public String getOrder_status() {
        return order_status;
    }

    public void setOrder_status(String order_status) {
        this.order_status = order_status;
    }
}
