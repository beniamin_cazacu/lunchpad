package com.example.benni.lunchpad.Fragments;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.example.benni.lunchpad.R;
import com.example.benni.lunchpad.Response.MenusList.CodeMenuResponse;
import com.example.benni.lunchpad.Retrofit.RestClient;
import com.example.benni.lunchpad.Singleton.MenuListSingleton;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link CodeScreenFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link CodeScreenFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CodeScreenFragment extends Fragment {

    TextInputEditText menuCode;
    Button buttonSendCode;

    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";

    private String mParam1;

    private OnFragmentInteractionListener callback;

    public CodeScreenFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @return A new instance of fragment CodeScreenFragment.
     */
    public static CodeScreenFragment newInstance(String param1) {
        CodeScreenFragment fragment = new CodeScreenFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_code_screen, container, false);
    }

    /**
     * When buttonSendCode is clicked , send Menu Code to server
     *
     * @param view               - VIEW
     * @param savedInstanceState - bundle savedInstanceState
     */
    @Override
    public void onViewCreated(final View view, Bundle savedInstanceState) {
        menuCode = (TextInputEditText) view.findViewById(R.id.code_screen_editTextCode);
        buttonSendCode = (Button) view.findViewById(R.id.code_screen_button_sendCode);
        buttonSendCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View _view) {
                // Validate
                if (menuCode.getText().toString().trim().length() != 0) {
                    String getMenuCode = menuCode.getText().toString();
                    RestClient.networkHandler().sendMenuCode(getMenuCode, new Callback<CodeMenuResponse>() {
                        @Override
                        public void success(CodeMenuResponse codeMenuResponse, Response response) {
                            // check if response is failure
                            if (codeMenuResponse.getFirst_dish() == null)
                                Toast.makeText(_view.getContext(), "No order", Toast.LENGTH_SHORT).show();
                            else {
                                // ensure communication with MainFragmentActivity
                                if (callback != null) {
                                    MenuListSingleton.getInstance().setCodeMenuResponse(codeMenuResponse);
                                    callback.onCodeScreenFragmentInteraction(_view);
                                }
                            }
                        }

                        @Override
                        public void failure(RetrofitError error) {
                            Toast.makeText(_view.getContext(), "Error get menu Code " + error, Toast.LENGTH_SHORT).show();
                        }
                    });
                } else {
                    menuCode.requestFocus();
                    Drawable myIcon = getResources().getDrawable(R.drawable.icon_invalid);
                    myIcon.setBounds(0, 0, myIcon.getIntrinsicWidth(), myIcon.getIntrinsicHeight());
                    menuCode.setError("INSERT A VALID CODE!", myIcon);
                }
            }
        });
    }

    /**
     * Called when the fragment is attached to the activity
     *
     * @param context - activity context
     */
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            callback = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    /**
     * Called when the fragment is unlinked to the activity
     */
    @Override
    public void onDetach() {
        super.onDetach();
        callback = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        void onCodeScreenFragmentInteraction(View view);
    }
}
