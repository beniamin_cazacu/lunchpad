package com.example.benni.lunchpad.Activity;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.example.benni.lunchpad.Fragments.UserDataFragment;
import com.example.benni.lunchpad.Models.UserLogin;
import com.example.benni.lunchpad.Models.UserInformation;
import com.example.benni.lunchpad.R;

/**
 * Created bye BENNI on 9/20/2016.
 * The registration Activity
 */
public class RegistrationActivity extends AppCompatActivity implements UserDataFragment.OnFragmentInteractionListener {

    UserDataFragment registrationFragment; // the registration fragment

    UserInformation userInformation; // contains all information of user
    UserLogin userLogin; // used for sending information logging
    /**
     * Initializes facebook login
     * Initializes data
     *
     * @param savedInstanceState
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        // Get name an email , if exist , from bundle. Used when user make facebook login
        Bundle bundle = getIntent().getExtras();
        if (bundle!=null){
            Boolean isFacebookRegistration = bundle.getBoolean("isFacebookRegistration");
            if (isFacebookRegistration){
                String name = bundle.getString("name");
                String email = bundle.getString("email");

                // for populate with data from facebook
                registrationFragment = UserDataFragment.newInstance("facebookRegistration",name,email);
                FragmentManager fragmentManager = getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.add(R.id.registerActivity_fragmentContainer, registrationFragment);
                fragmentTransaction.commit();
            }
        } else{
            registrationFragment = UserDataFragment.newInstance("registration");
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.add(R.id.registerActivity_fragmentContainer, registrationFragment);
            fragmentTransaction.commit();
        }
        userLogin = new UserLogin();
        userInformation = new UserInformation();
    }
    @Override
    public void onUserDataFragmentInteraction(View view) {}
}
