package com.example.benni.lunchpad.Tests;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.benni.lunchpad.Models.UserInformation;
import com.example.benni.lunchpad.Models.LoginModels.UserLogin;
import com.example.benni.lunchpad.R;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

/**
 * Created bye BENNI on 9/13/2016.
 */
public class RegistrationActivityOLD extends Activity implements View.OnClickListener {

    UserInformation userInformation; // contains all information of user
    UserLogin userLogin;

    Boolean checkFacebook = false;

    private CallbackManager callbackManager;
    Button registerButton;
    LoginButton loginFacebookButton;

    TextInputEditText editEmail, editName;
    TextInputEditText editPassword;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (checkFacebook == false) {

            FacebookSdk.sdkInitialize(getApplicationContext());
            AppEventsLogger.activateApp(this);
            callbackManager = CallbackManager.Factory.create();
        }

        setContentView(R.layout.activity_registration);

        userInformation = new UserInformation();
        userLogin = new UserLogin();

        //registerButton = (Button) findViewById(R.id.registration_signUpButton);
       // registerButton.setOnClickListener(this);

        if (checkFacebook == false) {

            loginFacebookButton = (LoginButton) findViewById(R.id.register_facebookButton);
            loginFacebookButton.setVisibility(View.VISIBLE);
            loginFacebookButton.setOnClickListener(this);

            //loginFacebookButton = (LoginButton)findViewById(R.id.login_facebookButton);
            logInFacebook();

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    private void logInFacebook() {
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "com.example.benni.lunchpad",
                    PackageManager.GET_SIGNATURES);
            for (android.content.pm.Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }

        loginFacebookButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
//                test.setText(
//                        "User ID: "
//                                + loginResult.getAccessToken().getUserId()
//                                + "\n" +
//                                "Auth Token: "
//                                + loginResult.getAccessToken().getToken()
//                );
                GraphRequest request = GraphRequest.newMeRequest(
                        AccessToken.getCurrentAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {

                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {
                                Log.v("LoginActivity", response.toString());
// Application code
                                try {
                                    editName = (TextInputEditText) findViewById(R.id.register_name);
                                    editEmail = (TextInputEditText) findViewById(R.id.register_email);
                                    editName.setText(object.getString("name"));
                                    editEmail.setText(object.getString("email"));


                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
//
                            }
                        }
                );
                Bundle parameters = new Bundle();
                parameters.putString("fields", "email,name");
                request.setParameters(parameters);
                request.executeAsync();


                // LOGOUT FACEBOOK
                logOutFacebook();

            }


            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {

            }
        });
    }

    private void logOutFacebook() {
//        GraphRequest deletePermanentRequest = new GraphRequest(AccessToken.getCurrentAccessToken(), "/me/permissions/", null, HttpMethod.DELETE, new GraphRequest.Callback() {
//
//            @Override
//            public void onCompleted(GraphResponse response) {
//                if (response != null) {
//                    FacebookRequestError error = response.getError();
//                    if (error != null) {
//                        Log.e("Deleting", error.toString());
//                    } else {
//                        finish();
//                    }
//                }
//                checkFacebook = true;
//            }
//        });
//        Log.d("Deleting", "Executing revoke permissions with graph path" + deletePermanentRequest.getGraphPath());
        LoginManager.getInstance().logOut();
        loginFacebookButton.setVisibility(View.INVISIBLE);
      //  deletePermanentRequest.executeAsync();
    }

    public boolean Valid() {

        final EditText nameValidate = (EditText) findViewById(R.id.register_name);
        String name = nameValidate.getText().toString().trim();
        String namePattern = "^[\\p{L} '-]+$";

        final EditText emailValidate = (EditText) findViewById(R.id.register_email);
        String email = emailValidate.getText().toString().trim();
        String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

        final EditText passwordValidate = (EditText) findViewById(R.id.register_password);
        String password = passwordValidate.getText().toString().trim();
        String passwordPattern = ".*\\d+.*";

        final EditText repasswordValidate = (EditText) findViewById(R.id.register_rePassword);
        String repassword = repasswordValidate.getText().toString().trim();


        final EditText numbervalidate = (EditText) findViewById(R.id.register_phoneNumber);
        String phone = numbervalidate.getText().toString().trim();
        String regexStr = "^[\\+[0-9]]{10,13}$";

        final EditText addressValidate = (EditText) findViewById(R.id.register_address);
        String address = addressValidate.getText().toString().trim();

        if (name.isEmpty()) {
            nameValidate.requestFocus();
            Drawable myIcon = getResources().getDrawable(R.drawable.icon_invalid);
            myIcon.setBounds(0, 0, myIcon.getIntrinsicWidth(), myIcon.getIntrinsicHeight());
            nameValidate.setError("NAME CANNOT BE EMPTY", myIcon);
            return false;
        }
        if (!name.matches(namePattern)) {
            nameValidate.requestFocus();
            Drawable myIcon = getResources().getDrawable(R.drawable.icon_invalid);
            myIcon.setBounds(0, 0, myIcon.getIntrinsicWidth(), myIcon.getIntrinsicHeight());
            nameValidate.setError("NAME CANNOT CONTAIN NUMBERS", myIcon);
            return false;
        }
        if (name.length() < 5 || !name.contains(" ")) {
            nameValidate.requestFocus();
            Drawable myIcon = getResources().getDrawable(R.drawable.icon_invalid);
            myIcon.setBounds(0, 0, myIcon.getIntrinsicWidth(), myIcon.getIntrinsicHeight());
            nameValidate.setError("NAME MUST BE: FIRST_NAME LAST NAME");
            return false;
        }
        if (email.isEmpty()) {
            emailValidate.requestFocus();
            Drawable myIcon = getResources().getDrawable(R.drawable.icon_invalid);
            myIcon.setBounds(0, 0, myIcon.getIntrinsicWidth(), myIcon.getIntrinsicHeight());
            emailValidate.setError("EMAIL CANNOT BE EMPTY", myIcon);
            return false;
        }
        if (!email.matches(emailPattern)) {
            emailValidate.requestFocus();
            Drawable myIcon = getResources().getDrawable(R.drawable.icon_invalid);
            myIcon.setBounds(0, 0, myIcon.getIntrinsicWidth(), myIcon.getIntrinsicHeight());
            emailValidate.setError("WRONG EMAIL", myIcon);
            return false;
        }
        if (password.isEmpty()) {
            passwordValidate.requestFocus();
            Drawable myIcon = getResources().getDrawable(R.drawable.icon_invalid);
            myIcon.setBounds(0, 0, myIcon.getIntrinsicWidth(), myIcon.getIntrinsicHeight());
            passwordValidate.setError("PASSWORD CANNOT BE EMPTY", myIcon);
            return false;
        }
        if (password.length() < 6) {
            passwordValidate.requestFocus();
            Drawable myIcon = getResources().getDrawable(R.drawable.icon_invalid);
            myIcon.setBounds(0, 0, myIcon.getIntrinsicWidth(), myIcon.getIntrinsicHeight());
            passwordValidate.setError("Password must have 6 characters ", myIcon);
            return false;
        }
        if (!password.matches(passwordPattern)) {
            Drawable myIcon = getResources().getDrawable(R.drawable.icon_invalid);
            myIcon.setBounds(0, 0, myIcon.getIntrinsicWidth(), myIcon.getIntrinsicHeight());
            passwordValidate.setError("Password must contain at least one number", myIcon);
            return false;
        }

        if (repassword.isEmpty()) {
            repasswordValidate.requestFocus();
            Drawable myIcon = getResources().getDrawable(R.drawable.icon_invalid);
            myIcon.setBounds(0, 0, myIcon.getIntrinsicWidth(), myIcon.getIntrinsicHeight());
            repasswordValidate.setError("REPASSWORD CANNOT BE EMPTY", myIcon);
            return false;
        }
        if (!password.equals(repassword)) {
            repasswordValidate.requestFocus();
            Drawable myIcon = getResources().getDrawable(R.drawable.icon_invalid);
            myIcon.setBounds(0, 0, myIcon.getIntrinsicWidth(), myIcon.getIntrinsicHeight());
            repasswordValidate.setError("Password does not match!", myIcon);
            return false;
        }

        if (phone.isEmpty()) {
            numbervalidate.requestFocus();
            Drawable myIcon = getResources().getDrawable(R.drawable.icon_invalid);
            myIcon.setBounds(0, 0, myIcon.getIntrinsicWidth(), myIcon.getIntrinsicHeight());
            numbervalidate.setError("PHONE NUMBER CANNOT BE EMPTY", myIcon);
            return false;
        }
        if (!phone.matches(regexStr)) {
            numbervalidate.requestFocus();
            Drawable myIcon = getResources().getDrawable(R.drawable.icon_invalid);
            myIcon.setBounds(0, 0, myIcon.getIntrinsicWidth(), myIcon.getIntrinsicHeight());
            numbervalidate.setError("INVALID NUMBER : must contains 10 - 13 characters", myIcon);
            return false;
        }
        if (address.isEmpty()) {
            addressValidate.requestFocus();
            Drawable myIcon = getResources().getDrawable(R.drawable.icon_invalid);
            myIcon.setBounds(0, 0, myIcon.getIntrinsicWidth(), myIcon.getIntrinsicHeight());
            addressValidate.setError("ADDRESS CANNOT BE EMPTY", myIcon);
            return false;
        }
        if (address.length() < 5) {
            addressValidate.requestFocus();
            Drawable myIcon = getResources().getDrawable(R.drawable.icon_invalid);
            myIcon.setBounds(0, 0, myIcon.getIntrinsicWidth(), myIcon.getIntrinsicHeight());
            addressValidate.setError("ADDRESS MUST CONTAIN AT LEAST 5 CHARACTERS", myIcon);
            return false;
        }

        return true;
    }

    @Override
    public void onClick(View view) {
       // if (view.getId() == R.id.registration_signUpButton) {
       //     RegistrationClicked(view);
      //  }
        if (checkFacebook == false) {
            if (view.getId() == R.id.register_facebookButton) {
                LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "email"));  // name , email , picture, password
//        }
            }
        }
    }

    private void RegistrationClicked(View view) {
        if (Valid()) {
            final TextInputEditText editName = (TextInputEditText) findViewById(R.id.register_name);
            editEmail = (TextInputEditText) findViewById(R.id.register_email);
            editPassword = (TextInputEditText) findViewById(R.id.register_password);
            final TextInputEditText editPasswordConfirmation = (TextInputEditText) findViewById(R.id.register_rePassword);
            final TextInputEditText editPhoneNumber = (TextInputEditText) findViewById(R.id.register_phoneNumber);
            final TextInputEditText editAddress = (TextInputEditText) findViewById(R.id.register_address);

            // populate User Information with data
            userInformation.setName(editName.getText().toString());
            userInformation.setEmail(editEmail.getText().toString());
            userInformation.setPassword(editPassword.getText().toString());
            userInformation.setPassword_confirmation(editPasswordConfirmation.getText().toString());
            userInformation.setPhone(editPhoneNumber.getText().toString());
            userInformation.setAddress(editAddress.getText().toString());

//            RestClient.networkHandler().sendDataRegister(userInformation, new Callback<UserLogin>() {
//                @Override
//                public void success(UserLogin s, Response response) {
//                    // Toast.makeText(Registration.this, "SUCCES! Reg ", Toast.LENGTH_SHORT).show();
//                    //s.setEmail(userInformation.getEmail());
//                    // s.setPassword(userInformation.getPassword());
//                    // User.getInstance().setUserLogin(s);
//
//                    Login();
//
//                    //  Intent intent = new Intent (Registration.this, MainActivity.class);
//                    //  intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                    //  startActivity(intent);
//                }
//
//                @Override
//                public void failure(RetrofitError error) {
//                    Toast.makeText(RegistrationActivity.this, "ERROR REG " + error, Toast.LENGTH_SHORT).show();
//                    error.printStackTrace();
//                }
//            });


        }
    }

    private void Login() {
        //final TextInputEditText editEmail = (TextInputEditText) findViewById(R.id.login_editEmail);
        //final TextInputEditText editPassword = (TextInputEditText) findViewById(R.id.login_editPassword);

        userLogin.setEmail(editEmail.getText().toString());
        userLogin.setPassword(editPassword.getText().toString());
//        RestClient.networkHandler().sendDataLogin(userLogin, new Callback<JSONObject>() {
//
//
//            @Override
//            public void success(JSONObject jsonObject, Response response) {
//                Toast.makeText(RegistrationActivity.this, "SUCCES! ", Toast.LENGTH_SHORT).show();
////                    User.getInstance().setUserLogin(s);
//                Intent intent = new Intent(RegistrationActivity.this, MenuList.class);
//                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                startActivity(intent);
//            }
//
//            @Override
//            public void failure(RetrofitError error) {
//                Toast.makeText(RegistrationActivity.this, "ERROR AUTOLOGIN" + error, Toast.LENGTH_SHORT).show();
//                error.printStackTrace();
//            }
//
//
//        });
    }
}
