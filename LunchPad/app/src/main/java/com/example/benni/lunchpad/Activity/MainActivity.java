package com.example.benni.lunchpad.Activity;

import android.app.Application;
import android.content.Intent;
import android.widget.Toast;

import com.example.benni.lunchpad.Models.DeviceIDModel;
import com.example.benni.lunchpad.Models.UserInformation;
import com.example.benni.lunchpad.Retrofit.RestClient;
import com.example.benni.lunchpad.Singleton.UserSingleton;
import com.onesignal.OneSignal;

import org.json.JSONObject;

import java.util.HashMap;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created bye BENNI on 9/22/2016.
 * Used for AutoLogin and push notification
 */

public class MainActivity extends Application {

    // User session Manager class
    UserSessionManager userSessionManager; // manage user session
    String token;

    /**
     * Chech @UserSessionManager if exist token stored and if IS_USER_LOGIN is true
     * If yes, then make autologin
     */
    @Override
    public void onCreate() {
        super.onCreate();
        OneSignal.startInit(this).init(); // for push notification
        userSessionManager = new UserSessionManager(getApplicationContext());

        // get user data from session
        HashMap<String, String> user = userSessionManager.getUserDetails();

        // get token
        token = user.get(UserSessionManager.KEY_TOKEN);

        UserSingleton.getInstance().setToken(token);

        // Check user login
        // If User is not logged in , This will redirect user to MainActivity.
        if (!userSessionManager.checkLogin()) {
            // make callback to get user data
            RestClient.networkHandler().getUserProfile(token, new Callback<UserInformation>() {
                @Override
                public void success(UserInformation userInformation, Response response) {
                    UserSingleton.getInstance().setUserInformation(userInformation);
                    UserSingleton.getInstance().setToken(token);
                    Intent intent = new Intent(MainActivity.this, MainFragmentActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                }

                @Override
                public void failure(RetrofitError error) {
                    Toast.makeText(MainActivity.this, "ERROR GET USER DATA" + error, Toast.LENGTH_SHORT).show();
                    error.printStackTrace();
                }
            });
            // make callback to send device ID

            OneSignal.idsAvailable(new OneSignal.IdsAvailableHandler(){

                @Override
                public void idsAvailable(String userId, String registrationId) {
                    //oneSignalAppId = userId;
                  //  UserSingleton.getInstance().setToken("RnAZ-9MoQDucwajZkp5N");
                    DeviceIDModel deviceIDModel = new DeviceIDModel(userId,UserSingleton.getInstance().getToken());
                   RestClient.networkHandler().sendDeviceId(deviceIDModel, new Callback<JSONObject>() {
                       @Override
                       public void success(JSONObject jsonObject, Response response) {
                           Toast.makeText(MainActivity.this, "Device Id sent successfully", Toast.LENGTH_SHORT).show();
                       }
                       @Override
                       public void failure(RetrofitError error) {
                           Toast.makeText(MainActivity.this, "Device Id sent failure", Toast.LENGTH_SHORT).show();
                       }
                   });
                }
            });
        }
    }
}
