package com.example.benni.lunchpad.Models;

/**
 * Created bye BENNI on 10/7/2016.
 * Used to send device ID to server, for push notification
 */

public class DeviceIDModel {
    private String device_token;
    private String token;

    public DeviceIDModel(){}
    public DeviceIDModel(String device_token, String token) {
        this.device_token = device_token;
        this.token = token;
    }

    public String getDevice_token() {
        return device_token;
    }

    public void setDevice_token(String device_token) {
        this.device_token = device_token;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
