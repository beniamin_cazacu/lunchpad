package com.example.benni.lunchpad.Activity;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.benni.lunchpad.Fragments.UserDataFragment;
import com.example.benni.lunchpad.Models.UserInformation;
import com.example.benni.lunchpad.Models.UserLogin;
import com.example.benni.lunchpad.R;
import com.example.benni.lunchpad.Response.LoginResponse;
import com.example.benni.lunchpad.Retrofit.RestClient;
import com.example.benni.lunchpad.Singleton.UserSingleton;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Activity for login view
 */

public class LoginActivity extends AppCompatActivity implements View.OnClickListener,UserDataFragment.OnFragmentInteractionListener{

    private static boolean activityStarted;


    private CallbackManager callbackManager; // Used for Facebook callback

    TextView textRegister; // TextView for register
    Button loginButton; // Button for login


    UserLogin userLogin; // information needed for authentication
    LoginButton loginFacebookButton; // login with facebook - button

    UserSessionManager userSessionManager; // manage userSession

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create(); // instance of callbackManager

        setContentView(R.layout.activity_login);

        textRegister = (TextView) findViewById(R.id.login_textRegister);
        textRegister.setOnClickListener(this);

        loginButton = (Button) findViewById(R.id.login_button);
        loginButton.setOnClickListener(this);

        userLogin = new UserLogin();

        loginFacebookButton = (LoginButton) findViewById(R.id.login_facebookButton);
        loginFacebookButton.setVisibility(View.VISIBLE);
        loginFacebookButton.setOnClickListener(this);

        logInFacebook();

    }

    /**
     * Tapping the login button starts off a new Activity, which returns a result
     * Used for receive and handle the result
     *
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    /**
     * Check if email is valid and if password is not empty
     * If something is not good , it will show an message error
     * Return true if fields are valid or false if not
     */
    public boolean Valid() {
        final TextInputEditText userValidate = (TextInputEditText) findViewById(R.id.login_editEmail);
        String user = userValidate.getText().toString().trim();
        String userPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+"; // allow just space and letters
        final TextInputEditText passwordValidate = (TextInputEditText) findViewById(R.id.login_editPassword);
        String password = passwordValidate.getText().toString().trim();
        if (user.isEmpty()) {
            userValidate.requestFocus();
            Drawable myIcon = getResources().getDrawable(R.drawable.icon_invalid);
            myIcon.setBounds(0, 0, myIcon.getIntrinsicWidth(), myIcon.getIntrinsicHeight());
            userValidate.setError("USER CANNOT BE EMPTY", myIcon);
            return false;
        }
        if (!user.matches(userPattern)) {  // Check if the email is valid
            userValidate.requestFocus();
            Drawable myIcon = getResources().getDrawable(R.drawable.icon_invalid);
            myIcon.setBounds(0, 0, myIcon.getIntrinsicWidth(), myIcon.getIntrinsicHeight());
            userValidate.setError("User is wrong!", myIcon);
            return false;
        }
        if (password.isEmpty()) {
            passwordValidate.requestFocus();
            Drawable myIcon = getResources().getDrawable(R.drawable.icon_invalid);
            myIcon.setBounds(0, 0, myIcon.getIntrinsicWidth(), myIcon.getIntrinsicHeight());
            passwordValidate.setError("PASSWORD CANNOT BE EMPTY", myIcon);
            return false;
        }
        return true;
    }

    /**
     * Event for click button
     * @param view
     */
    @Override
    public void onClick(View view) {

        if (view.getId() == R.id.login_button) {
            LoginClicked();
        }
        if (view.getId() == R.id.login_textRegister) {
            RegistrationClicked();
        }
        if (view.getId() == R.id.login_facebookButton) {
            LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "email"));  // name , email , picture, password
        }
//
    }

    /**
     * Start Registration activity
     */
    private void RegistrationClicked() { // Send user to Registration activity
        loginButton.setEnabled(false);
        Intent intent = new Intent(this, RegistrationActivity.class);
        startActivity(intent);
        loginButton.setEnabled(true);


    }

    /**
     * Make user login
     * If login successful , get user profile information and make userSessionManager to keep token for autologin
     */
    private void LoginClicked() {
        if (Valid()) {
            final TextInputEditText editEmail = (TextInputEditText) findViewById(R.id.login_editEmail);
            final TextInputEditText editPassword = (TextInputEditText) findViewById(R.id.login_editPassword);

            userSessionManager = new UserSessionManager(getApplicationContext());

            userLogin.setEmail(editEmail.getText().toString());
            userLogin.setPassword(editPassword.getText().toString());
            // Send information containing email and password , to server
            // If success  , set a message of success and start a new Activity
            // If not, set an error message
            RestClient.networkHandler().sendDataLogin(userLogin, new Callback<LoginResponse>() {

                @Override
                public void success(LoginResponse loginResponse, Response response) {
                    Toast.makeText(LoginActivity.this, "SUCCES! ", Toast.LENGTH_SHORT).show();
                    userSessionManager.createUserLoginSession(loginResponse.getAuth_token());
                    UserSingleton.getInstance().setToken(loginResponse.getAuth_token());
                    final String token = loginResponse.getAuth_token();
                    // make callback to get user data
                    RestClient.networkHandler().getUserProfile(token, new Callback<UserInformation>() {
                        @Override
                        public void success(UserInformation userInformation, Response response) {
                            UserSingleton.getInstance().setUserInformation(userInformation);
                            UserSingleton.getInstance().setToken(token);
                            Intent intent = new Intent(LoginActivity.this, MainFragmentActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                        }

                        @Override
                        public void failure(RetrofitError error) {
                            Toast.makeText(LoginActivity.this, "ERROR GET USER DATA" + error, Toast.LENGTH_SHORT).show();
                            error.printStackTrace();
                        }
                    });


                }

                @Override
                public void failure(RetrofitError error) {
                    Toast.makeText(LoginActivity.this, "ERROR AUTOLOGIN" + error, Toast.LENGTH_SHORT).show();
                    error.printStackTrace();
                }
            });
        }
    }
    /**
     * Get the KeyHash
     * Make request for Facebook Login
     */
    private void logInFacebook() {
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "com.example.benni.lunchpad",
                    PackageManager.GET_SIGNATURES);
            for (android.content.pm.Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }

        loginFacebookButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Toast.makeText(LoginActivity.this, "Login attempt succes", Toast.LENGTH_SHORT).show();
                // Get data from Facebook
                GraphRequest request = GraphRequest.newMeRequest(
                        AccessToken.getCurrentAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {
                                Log.v("LoginActivity", response.toString());
                                try {
                                    Intent intent = new Intent(LoginActivity.this,RegistrationActivity.class);
                                    intent.putExtra("isFacebookRegistration",true);
                                    intent.putExtra("name",object.getString("name"));
                                    intent.putExtra("email",object.getString("email"));
                                    startActivity(intent);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                );
                // set parameters for GraphRequest
                Bundle parameters = new Bundle();
                parameters.putString("fields", "email,name");
                request.setParameters(parameters);
                request.executeAsync();

                // LOGOUT FACEBOOK
                logOutFacebook();

            }
            public void onCancel() {
                Toast.makeText(LoginActivity.this, "Login attempt cancelled", Toast.LENGTH_SHORT).show();
            }
            @Override
            public void onError(FacebookException error) {
                Toast.makeText(LoginActivity.this, "Login attempt error " + error, Toast.LENGTH_SHORT).show();
            }
        });
    }

    /**
     * Logout from Facebook and hide facebook button
     */
    private void logOutFacebook() {
        LoginManager.getInstance().logOut();
        loginFacebookButton.setVisibility(View.INVISIBLE);
    }

    /**
     * Make connection with @UserDataFragment
     * @param view
     */
    @Override
    public void onUserDataFragmentInteraction(View view) {

    }
}
