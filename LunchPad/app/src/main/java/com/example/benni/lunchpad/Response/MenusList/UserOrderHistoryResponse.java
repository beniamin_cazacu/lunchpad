package com.example.benni.lunchpad.Response.MenusList;

/**
 * Created bye BENNI on 10/3/2016.
 */

public class UserOrderHistoryResponse {
    private String email;
    private String password;
    private String name;

    public UserOrderHistoryResponse(){}
    public UserOrderHistoryResponse(String email, String password, String name) {
        this.email = email;
        this.password = password;
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
