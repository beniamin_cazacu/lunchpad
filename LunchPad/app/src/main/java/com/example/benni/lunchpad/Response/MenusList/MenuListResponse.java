package com.example.benni.lunchpad.Response.MenusList;

import java.util.List;

/**
 * Created bye BENNI on 9/27/2016.
 */

public class MenuListResponse {
    private Integer id;
    private String title;
    private String date;
    private MenuRestaurantResponse restaurant;
    private List<MenuDishesResponse> dishes;

    public MenuListResponse(){}

    public MenuListResponse(Integer id, String title, String date, MenuRestaurantResponse restaurant, List<MenuDishesResponse> dishes) {
        this.id = id;
        this.title = title;
        this.date = date;
        this.restaurant = restaurant;
        this.dishes = dishes;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public MenuRestaurantResponse getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(MenuRestaurantResponse restaurant) {
        this.restaurant = restaurant;
    }

    public List<MenuDishesResponse> getDishes() {
        return dishes;
    }

    public void setDishes(List<MenuDishesResponse> dishes) {
        this.dishes = dishes;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
