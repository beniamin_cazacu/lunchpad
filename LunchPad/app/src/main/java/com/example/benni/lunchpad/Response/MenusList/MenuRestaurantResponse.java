package com.example.benni.lunchpad.Response.MenusList;

/**
 * Created bye BENNI on 9/27/2016.
 */

public class MenuRestaurantResponse {
    private Integer id;
    private String name;

    public MenuRestaurantResponse(){}

    public MenuRestaurantResponse(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
