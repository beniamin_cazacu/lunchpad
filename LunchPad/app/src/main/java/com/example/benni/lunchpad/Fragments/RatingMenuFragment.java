package com.example.benni.lunchpad.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RatingBar;
import android.widget.TextView;

import com.example.benni.lunchpad.R;
import com.example.benni.lunchpad.Response.MenusList.CodeMenuResponse;
import com.example.benni.lunchpad.Singleton.MenuListSingleton;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link RatingMenuFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link RatingMenuFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class RatingMenuFragment extends Fragment {

    TextView titleFirstDish, firstDish;
    TextView titleSecondDish, secondDish;
    TextView titleDessert, dessert;
    TextView menuStatus,titleMenuRating;
    Button sendRatingButton;

    RatingBar ratingBar;

    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";

    private String mParam1;

    private OnFragmentInteractionListener callback;

    public RatingMenuFragment() {
        // Required empty public constructor
    }
    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @return A new instance of fragment RatingMenuFragment.
     */
    public static RatingMenuFragment newInstance(String param1) {
        RatingMenuFragment fragment = new RatingMenuFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
        }
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_rating_menu, container, false);
    }

    /**
     * Instantiate needed variables
     * Get data menu and display it
     * Send rating information
     * @param view - view
     * @param savedInstanceState - bundle
     */
    @Override
    public void onViewCreated(final View view, Bundle savedInstanceState) {

        titleFirstDish = (TextView)view.findViewById(R.id.rating_textTitleFirstDish);
        firstDish = (TextView)view.findViewById(R.id.fragment_rating_firstDish);
        titleSecondDish = (TextView)view.findViewById(R.id.fragment_rating_textTitleSecondDish);
        secondDish = (TextView)view.findViewById(R.id.fragment_rating_secondDish);
        titleDessert = (TextView)view.findViewById(R.id.fragment_rating_textTitleDessert);
        dessert = (TextView)view.findViewById(R.id.fragment_rating_dessert);
        menuStatus = (TextView)view.findViewById(R.id.fragment_rating_status);
        titleMenuRating = (TextView)view.findViewById(R.id.fragment_rating_textRating);
        sendRatingButton = (Button)view.findViewById(R.id.fragment_rating_buttonOK);
        ratingBar = (RatingBar)view.findViewById(R.id.fragment_rating_ratingBar);


        // get data Menu from Singleton
        getDataMenu();
        sendRatingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View _view) {
                if (callback!=null){
                    // value of rating bar, rounded
                    Integer rating = Math.round(ratingBar.getRating());
                    callback.onRatingFragmentInteraction(_view,rating);
                }
            }
        });
    }

    /**
     * Get @codeMenuResonse form Singleton
     * if menu status is 'Send' , allow user to rate menu
     */
    private void getDataMenu() {
        CodeMenuResponse codeMenuResponse = MenuListSingleton.getInstance().getCodeMenuResponse();

        firstDish.setText(codeMenuResponse.getFirst_dish());
        secondDish.setText(codeMenuResponse.getSecond_dish());
        dessert.setText(codeMenuResponse.getDessert());
        menuStatus.setText(codeMenuResponse.getOrder_status());

        if (!menuStatus.getText().equals("Send")){
            titleMenuRating.setVisibility(View.GONE);
            ratingBar.setVisibility(View.GONE);
            sendRatingButton.setVisibility(View.GONE);
        }else {
            titleMenuRating.setVisibility(View.VISIBLE);
            ratingBar.setVisibility(View.VISIBLE);
        }
    }
    public void onButtonPressed(View view) {
        if (callback != null) {
            callback.onRatingFragmentInteraction(view,0);
        }
    }
    /**
     * Called when fragment is attached with activity
     * @param context - activity context
     */
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            callback = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }
    /**
     * Called when fragment is unlinked from the activity
     */
    @Override
    public void onDetach() {
        super.onDetach();
        callback = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onRatingFragmentInteraction(View view,Integer rating);
    }
}
