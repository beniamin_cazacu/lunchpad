package com.example.benni.lunchpad.Activity;


import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.NavigationView;
import android.support.design.widget.NavigationView.OnNavigationItemSelectedListener;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.example.benni.lunchpad.Fragments.CodeScreenFragment;
import com.example.benni.lunchpad.Fragments.MenuListFragment;
import com.example.benni.lunchpad.Fragments.OrderViewFragment;
import com.example.benni.lunchpad.Fragments.RatingMenuFragment;
import com.example.benni.lunchpad.Fragments.UserDataFragment;
import com.example.benni.lunchpad.Fragments.UserProfileFragment;
import com.example.benni.lunchpad.Models.MenuListModels.SendRatingModel;
import com.example.benni.lunchpad.R;
import com.example.benni.lunchpad.Retrofit.RestClient;
import com.example.benni.lunchpad.Singleton.MenuListSingleton;
import com.example.benni.lunchpad.Singleton.UserSingleton;

import org.json.JSONObject;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import static com.example.benni.lunchpad.Fragments.UserProfileFragment.OnFragmentInteractionListener;

/**
 * Created bye BENNI on 9/21/2016.
 * Manage all fragments
 */

public class MainFragmentActivity extends AppCompatActivity implements OnFragmentInteractionListener, OnNavigationItemSelectedListener, UserDataFragment.OnFragmentInteractionListener, MenuListFragment.OnFragmentInteractionListener, OrderViewFragment.OnFragmentInteractionListener, CodeScreenFragment.OnFragmentInteractionListener, RatingMenuFragment.OnFragmentInteractionListener {

    /**
     * Fragments
     */
    UserDataFragment userDataFragment;
    UserProfileFragment userProfileFragment;
    OrderViewFragment orderViewFragment;
    CodeScreenFragment codeScreenFragment;
    RatingMenuFragment ratingMenuFragment;

    UserSessionManager userSessionManager; // keep user session

    String picturePath; // keep the picture Path
    // number of images to select
    private static final int PICK_IMAGE = 1;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sidebar_user_profile);

        userSessionManager = new UserSessionManager(getApplicationContext());

        Toolbar toolbar = (Toolbar) findViewById(R.id.main_fragment_toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        orderViewFragment = OrderViewFragment.newInstance("order");
        fragmentTransaction.replace(R.id.menuListActivity_fragmentContainer, orderViewFragment);
        fragmentTransaction.commit();

        userProfileFragment = (UserProfileFragment) getSupportFragmentManager().findFragmentById(R.id.fragmentProfile);
        userProfileFragment.populateProfileFields();
    }

    /**
     * When drawerLayout is focused and back button is pressed , close drawerLayout
     */
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }
    /**
     * Called when code_screen_button_sendCode from CodeScreenFragment is pressed
     * Replace CodeSceenFragment with ratingMenuFragment
     *
     * @param view - view
     */
    @Override
    public void onCodeScreenFragmentInteraction(View view) {
        ratingMenuFragment = RatingMenuFragment.newInstance("rating");
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.menuListActivity_fragmentContainer, ratingMenuFragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }
    @Override
    public void onFragmentInteraction(Uri uri) {}
    /**
     * Called when fragment_rating_buttonOK from RatingMenuFragment is pressed
     * Send rating menu
     * Replace RatingMenuFragment with ratingMenuFragment with OrderViewFragment
     *
     * @param view - view
     */
    @Override
    public void onRatingFragmentInteraction(View view, Integer rating) {
        if (MenuListSingleton.getInstance().getCodeMenuResponse().getOrder_status().equals("Send")) {
            SendRatingModel sendRatingModel = new SendRatingModel();
            sendRatingModel.setToken(UserSingleton.getInstance().getToken());
            sendRatingModel.setOrder_id(MenuListSingleton.getInstance().getCodeMenuResponse().getOrder_id().toString());
            sendRatingModel.setRating(rating);
            RestClient.networkHandler().sendMenuRating(sendRatingModel, new Callback<JSONObject>() {
                @Override
                public void success(JSONObject jsonObject, Response response) {
                    Toast.makeText(MainFragmentActivity.this, "Rating sent successfully", Toast.LENGTH_SHORT).show();

                    FragmentManager fragmentManager = getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    orderViewFragment = OrderViewFragment.newInstance("order");
                    fragmentTransaction.replace(R.id.menuListActivity_fragmentContainer, orderViewFragment);
                    fragmentTransaction.commit();
                }

                @Override
                public void failure(RetrofitError error) {
                    Toast.makeText(MainFragmentActivity.this, "Failure at sending rating " + error, Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    /**
     * Called when image_profile is pressed
     * Call selectImageFromGallery method
     *
     * @param view
     */
    @Override
    public void onUserDataFragmentInteraction(View view) {
        selectImageFromGallery();
    }

    /**
     * Called when a menu item is pressed
     *
     * @param item
     * @return
     */
    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        if (id == R.id.nav_profile) {

            userDataFragment = UserDataFragment.newInstance("profile");
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.menuListActivity_fragmentContainer, userDataFragment);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
            userProfileFragment.populateProfileFields();

        } else if (id == R.id.nav_orders) {

            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            orderViewFragment = OrderViewFragment.newInstance("order");
            fragmentTransaction.replace(R.id.menuListActivity_fragmentContainer, orderViewFragment);
            fragmentTransaction.commit();

        } else if (id == R.id.nav_menu_code) {

            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            codeScreenFragment = CodeScreenFragment.newInstance("Menu code");
            fragmentTransaction.replace(R.id.menuListActivity_fragmentContainer, codeScreenFragment);
            fragmentTransaction.commit();

        } else if (id == R.id.nav_logOut) {

            userSessionManager.logoutUser();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    /**
     * Opens dialog picker, so the user can select image from the gallery. The
     * result is returned in the method <code>onActivityResult()</code>
     */
    public void selectImageFromGallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select picture"), PICK_IMAGE);
    }

    /**
     * Retrives the result returned from selecting image, by invoking the method
     * <code>selectImageFromGallery()</code>
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE && resultCode == RESULT_OK && null != data) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = {
                    MediaStore.Images.Media.DATA
            };
            Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            picturePath = cursor.getString(columnIndex);
            cursor.close();


            decodeFile(picturePath);
        }
    }

    /**
     * Get Path From URI
     *
     * @param contentURI
     * @return
     */
    private String getRealPathFromURI(Uri contentURI) {
        String result;
        Cursor cursor = getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) { // Source is Dropbox or other similar local file path
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }

    /**
     * The method decodes the image file to avoid out of memory issues. Sets the
     * selected image in to the ImageView.
     *
     * @param filePath - path of image
     */
    public void decodeFile(String filePath) {
        UserDataFragment.filePath = filePath;
        UserDataFragment.setImage(MainFragmentActivity.this);
    }
}
