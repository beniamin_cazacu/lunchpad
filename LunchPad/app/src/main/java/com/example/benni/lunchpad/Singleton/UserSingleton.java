package com.example.benni.lunchpad.Singleton;

import com.example.benni.lunchpad.Models.UserLogin;
import com.example.benni.lunchpad.Models.UserInformation;

/**
 * Created bye BENNI on 9/14/2016.
 */
public class UserSingleton {

    private UserInformation userInformation = new UserInformation();

    private UserLogin userLogin = new UserLogin();



    private static UserSingleton ourInstance = new UserSingleton();

    public static UserSingleton getInstance() {
        return ourInstance;
    }


    private UserSingleton() {
    }
    public void setUserInformation(UserInformation userInformation) {
        this.userInformation = userInformation;
    }
    public UserInformation getUserInformation(){
        return userInformation;
    }

    public String getToken(){
        return userInformation.getToken();
    }

    public void setToken(String _token){
        userInformation.setToken(_token);
    }

    public UserLogin getUserLogin(){
        return userLogin;
    }
    public void setUserLogin(UserLogin userLogin){
         this.userLogin = userLogin;
    }


}
