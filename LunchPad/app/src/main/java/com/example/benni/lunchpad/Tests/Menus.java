package com.example.benni.lunchpad.Tests;

import android.support.annotation.NonNull;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

/**
 * Created bye BENNI on 9/15/2016.
 */
public class Menus {
    private Integer position;
    private Integer id;
    private String date;
    private String title;
    private String first_dish;
    private String second_dish;
    private String dessert;
    private String auth_token;
    private List<Integer>arrId = new List<Integer>() {
        @Override
        public int size() {
            return 0;
        }

        @Override
        public boolean isEmpty() {
            return false;
        }

        @Override
        public boolean contains(Object o) {
            return false;
        }

        @NonNull
        @Override
        public Iterator<Integer> iterator() {
            return null;
        }

        @NonNull
        @Override
        public Object[] toArray() {
            return new Object[0];
        }

        @NonNull
        @Override
        public <T> T[] toArray(T[] ts) {
            return null;
        }

        @Override
        public boolean add(Integer integer) {
            return false;
        }

        @Override
        public boolean remove(Object o) {
            return false;
        }

        @Override
        public boolean containsAll(Collection<?> collection) {
            return false;
        }

        @Override
        public boolean addAll(Collection<? extends Integer> collection) {
            return false;
        }

        @Override
        public boolean addAll(int i, Collection<? extends Integer> collection) {
            return false;
        }

        @Override
        public boolean removeAll(Collection<?> collection) {
            return false;
        }

        @Override
        public boolean retainAll(Collection<?> collection) {
            return false;
        }

        @Override
        public void clear() {

        }

        @Override
        public Integer get(int i) {
            return null;
        }

        @Override
        public Integer set(int i, Integer integer) {
            return null;
        }

        @Override
        public void add(int i, Integer integer) {

        }

        @Override
        public Integer remove(int i) {
            return null;
        }

        @Override
        public int indexOf(Object o) {
            return 0;
        }

        @Override
        public int lastIndexOf(Object o) {
            return 0;
        }

        @Override
        public ListIterator<Integer> listIterator() {
            return null;
        }

        @NonNull
        @Override
        public ListIterator<Integer> listIterator(int i) {
            return null;
        }

        @NonNull
        @Override
        public List<Integer> subList(int i, int i1) {
            return null;
        }


    };

    public Integer getPosition() {
        return position;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getFirst_dish() {
        return first_dish;
    }

    public void setFirst_dish(String first_dish) {
        this.first_dish = first_dish;
    }

    public String getSecond_dish() {
        return second_dish;
    }

    public void setSecond_dish(String second_dish) {
        this.second_dish = second_dish;
    }

    public String getDessert() {
        return dessert;
    }

    public void setDessert(String dessert) {
        this.dessert = dessert;
    }

    public String getAuth_token() {
        return auth_token;
    }

    public void setAuth_token(String auth_token) {
        this.auth_token = auth_token;
    }

    public List<Integer> getArrId() {
        return arrId;
    }

    public void setArrId(List<Integer> arrId) {
        this.arrId = arrId;
    }


}
