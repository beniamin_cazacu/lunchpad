package com.example.benni.lunchpad.Tests;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ListView;

import com.example.benni.lunchpad.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created bye BENNI on 9/13/2016.
 * Activity for list Menus
 */
public class MenuListDetailOLD extends Activity {

    public List<Integer> id; // id menu

    private ListView listViewMenu; // ListView used for displaying menus
    private MenuListAdapterOLD adapter; // adapter for populating list
    private List<MenuOLD> menuOLDList; // stores menu list from the server


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.old_listview_menus);

        listViewMenu = (ListView) findViewById(R.id.menuListView);
        menuOLDList = new ArrayList<>();

        // Request menu list from server
        // If success , populate ListView with necessary information
        // else,
//        RestClient.networkHandler().getMenuList("GAx9xa3MZaRJYB1FX-z8", new Callback<List<Menus>>(){
//
//            @Override
//            public void success(List<Menus> menuLists, Response response) {
//                for (int i = 0 ; i<menuLists.size();i++) {
//                    // insert into list, data requested from server
//                    menuList.add(new Menu(menuLists.get(i).getId(),menuLists.get(i).getTitle(), menuLists.get(i).getFirst_dish(), menuLists.get(i).getSecond_dish(), menuLists.get(i).getDessert()));
//                  //  menuLists.get(i).getArrId().add(menuLists.get(i).getId());
//                    //Menus.getInstance().setCurentMenu(menuLists.get(i).getId(),menuLists.get(i).getTitle(), menuLists.get(i).getFirst_dish(), menuLists.get(i).getSecond_dish(), menuLists.get(i).getDessert());
//                }
//                // populate listView
//                adapter = new MenuListAdapter(getApplicationContext(),menuList);
//                listViewMenu.setAdapter(adapter);
//            }
//
//            @Override
//            public void failure(RetrofitError error) {
//                Toast.makeText(MenuListDetail.this, "ERROR " + error, Toast.LENGTH_SHORT).show();
//                error.printStackTrace();
//            }
//        });


//        listViewMenu.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                Toast.makeText(getApplicationContext(), "Clicked product id =" + view.getTag(), Toast.LENGTH_SHORT).show();
//            }
//        });

    }


}
