package com.example.benni.lunchpad.Models.MenuListModels;

/**
 * Created bye BENNI on 9/29/2016.
 * Used to send Standard menu
 */

public class SendMenuStandardModel {
    private String token;
    private Integer dish1_id;
    private Integer dish2_id;
    private Integer dessert_id;

    public SendMenuStandardModel(){
    }
    public SendMenuStandardModel(String token, Integer dish1_id, Integer dish2_id, Integer dessert_id) {
        this.token = token;
        this.dish1_id = dish1_id;
        this.dish2_id = dish2_id;
        this.dessert_id = dessert_id;
    }

    public Integer getDish1_id() {
        return dish1_id;
    }

    public void setDish1_id(Integer dish1_id) {
        this.dish1_id = dish1_id;
    }

    public Integer getDish2_id() {
        return dish2_id;
    }

    public void setDish2_id(Integer dish2_id) {
        this.dish2_id = dish2_id;
    }

    public Integer getDessert_id() {
        return dessert_id;
    }

    public void setDessert_id(Integer dessert_id) {
        this.dessert_id = dessert_id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
