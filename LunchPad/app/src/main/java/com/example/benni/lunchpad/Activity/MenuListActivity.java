package com.example.benni.lunchpad.Activity;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.benni.lunchpad.Adapters.MenuAdapter;
import com.example.benni.lunchpad.Fragments.OrderViewFragment;
import com.example.benni.lunchpad.Models.MenuListModels.MenuModel;
import com.example.benni.lunchpad.Models.MenuListModels.SendMenuFitnessModel;
import com.example.benni.lunchpad.Models.MenuListModels.SendMenuStandardModel;
import com.example.benni.lunchpad.R;
import com.example.benni.lunchpad.Response.MenusList.MenuListResponse;
import com.example.benni.lunchpad.Response.MenusList.OrderHistoryResponse;
import com.example.benni.lunchpad.Retrofit.RestClient;
import com.example.benni.lunchpad.Singleton.MenuListSingleton;
import com.example.benni.lunchpad.Singleton.UserSingleton;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created bye BENNI on 9/27/2016.
 * Activity with all menu list
 */

public class MenuListActivity extends AppCompatActivity implements OrderViewFragment.OnFragmentInteractionListener {

    /**
     * Lists that keeps informations for menus
     */
    private List<MenuModel> menuListStandardFirstDish;
    private List<MenuModel> menuListStandardSecondDish;
    private List<MenuModel> menuListStandardDessert;
    private List<MenuModel> menuListFitness;

    /**
     * Adapters
     */
    private MenuAdapter standardFirstDishAdapter;
    private MenuAdapter standardSecondDishAdapter;
    private MenuAdapter standardDessertAdapter;
    private MenuAdapter fitnessAdapter;

    /**
     * GridViews that shows menus information
     */
    GridView gridViewStandardFirstDish;
    GridView gridviewStandardSecondDish;
    GridView gridViewStandardDessert;
    GridView gridViewFitnessMenu;

    ImageView checkedImage; // used for check or unCheck menu

    String restaurantName = "RESTAURANT";

    LinearLayout linearView; // LinearLayout that contains informations for users order history

    Boolean hideMenu = false; // if true, hide menu - user cannot make order

    /**
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu_list_activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.menu_list_toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitleTextColor(getResources().getColor(R.color.colorTextLight));

        getSupportActionBar().setHomeButtonEnabled(true); // enable Back button
        getSupportActionBar().setDisplayHomeAsUpEnabled(true); // show Back button

        menuListStandardFirstDish = new ArrayList<>();
        menuListStandardSecondDish = new ArrayList<>();
        menuListStandardDessert = new ArrayList<>();
        menuListFitness = new ArrayList<>();

        checkedImage = (ImageView) findViewById(R.id.menu_list_checkMenuImage);

        linearView = (LinearLayout) findViewById(R.id.menu_list_LinearView);

        gridViewStandardFirstDish = (GridView) findViewById(R.id.gridview_standard_first_dish);
        gridviewStandardSecondDish = (GridView) findViewById(R.id.gridview_standard_second_dish);
        gridViewStandardDessert = (GridView) findViewById(R.id.gridview_standard_dessert);
        gridViewFitnessMenu = (GridView) findViewById(R.id.gridview_menu_fitness);

        if (MenuListSingleton.getInstance().getMenu_status().equals("OPEN")) // check status Menus . If open , show menu , else : hide menu
            hideMenu = false;
        else
            hideMenu = true;
        invalidateOptionsMenu(); // refresh onCreateOptionsMenu method

        // get menus information from server
        RestClient.networkHandler().getMenuList(UserSingleton.getInstance().getToken(), MenuListSingleton.getInstance().getMenu_date(), new Callback<List<MenuListResponse>>() {
            @Override
            public void success(List<MenuListResponse> menuListResponse, Response response) {
                linearView.setVisibility(View.VISIBLE);
                restaurantName = menuListResponse.get(0).getRestaurant().getName();
                getSupportActionBar().setTitle(restaurantName);
                for (int i = 0; i < menuListResponse.size(); i++) {
                    for (int j = 0; j < menuListResponse.get(i).getDishes().size(); j++) {
                        Integer id = menuListResponse.get(i).getDishes().get(j).getId();
                        String menuTitle = menuListResponse.get(i).getDishes().get(j).getDish_title();
                        String imagePath = menuListResponse.get(i).getDishes().get(j).getPicture_url();
                        String menuType = menuListResponse.get(i).getDishes().get(j).getDish_type();
                        // Check if menu is standard(i=0) or is fitness(i=1)
                        if (i == 0) {
                            switch (menuType) {
                                case "First":
                                    menuListStandardFirstDish.add(new MenuModel(id, menuTitle, imagePath));
                                    break;
                                case "Second":
                                    menuListStandardSecondDish.add(new MenuModel(id, menuTitle, imagePath));
                                    break;
                                case "Dessert":
                                    menuListStandardDessert.add(new MenuModel(id, menuTitle, imagePath));
                                    break;
                                default:
                                    Toast.makeText(getApplicationContext(), "Error at type menu  ", Toast.LENGTH_LONG).show();
                            }
                        } else {
                            menuListFitness.add(new MenuModel(id, menuTitle, imagePath));
                        }
                    }
                }

                // initializes adapters
                standardFirstDishAdapter = new MenuAdapter(MenuListActivity.this, menuListStandardFirstDish);
                standardSecondDishAdapter = new MenuAdapter(getApplicationContext(), menuListStandardSecondDish);
                standardDessertAdapter = new MenuAdapter(getApplicationContext(), menuListStandardDessert);
                fitnessAdapter = new MenuAdapter(getApplicationContext(), menuListFitness);

                // the tags are used to identify menus
                gridViewStandardFirstDish.setTag("First Dish");
                gridviewStandardSecondDish.setTag("Second Dish");
                gridViewStandardDessert.setTag("Dessert");
                gridViewFitnessMenu.setTag("Fitness");

                //set adapters
                gridViewStandardFirstDish.setAdapter(standardFirstDishAdapter);
                gridviewStandardSecondDish.setAdapter(standardSecondDishAdapter);
                gridViewStandardDessert.setAdapter(standardDessertAdapter);
                gridViewFitnessMenu.setAdapter(fitnessAdapter);

                // set gridViews height to Match parent
                setGridViewHeight(gridViewStandardFirstDish);
                setGridViewHeight(gridviewStandardSecondDish);
                setGridViewHeight(gridViewStandardDessert);
                setGridViewHeight(gridViewFitnessMenu);
            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(getApplicationContext(), "FAILURE LIST " + error, Toast.LENGTH_SHORT).show();
            }
        });

        // take command history from server
        // set the information taken into layout and inflate that layout until all information is displayed
        RestClient.networkHandler().getOrderUsersHistory(MenuListSingleton.getInstance().getMenu_date(), new Callback<List<OrderHistoryResponse>>() {
            @Override
            public void success(List<OrderHistoryResponse> orderHistoryResponse, Response response) {
                LayoutInflater layoutInflater;
                LinearLayout linearLayout;

                layoutInflater = LayoutInflater.from(MenuListActivity.this);
                linearLayout = (LinearLayout) findViewById(R.id.menu_list_linearLayout);

                for (int i = 0; i < orderHistoryResponse.size(); i++) {
                    final View view = layoutInflater.inflate(R.layout.activity_menu_list_users_history_menus, null);
                    TextView name = (TextView) view.findViewById(R.id.activity_menu_list_history_name);
                    TextView firstDish_title = (TextView) view.findViewById(R.id.activity_menu_list_history_firsDish);
                    TextView secondDish_title = (TextView) view.findViewById(R.id.activity_menu_list_history_secondDish);
                    TextView dessert_title = (TextView) view.findViewById(R.id.activity_menu_list_history_dessert);

                    name.setText(orderHistoryResponse.get(i).getUser().getName());
                    firstDish_title.setText("• " + orderHistoryResponse.get(i).getDish1_title());
                    secondDish_title.setText("• " + orderHistoryResponse.get(i).getDish2_title());
                    dessert_title.setText("• " + orderHistoryResponse.get(i).getDessert_title());

                    linearLayout.addView(view);
                    // a user cannot have multiples order
                    if (orderHistoryResponse.get(i).getUser().getEmail().equals(UserSingleton.getInstance().getUserInformation().getEmail())) {
                        hideMenu = true;
                        MenuListSingleton.getInstance().setMenu_status("CLOSED");
                        invalidateOptionsMenu(); // refresh onCreateOptionsMenu method
                    }
                }
            }
            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(MenuListActivity.this, "Failure getting order user history " + error, Toast.LENGTH_SHORT).show();
            }
        });
    }

/**
 * Set height gridviews to match parent
 */
    private void setGridViewHeight(final GridView _gridView) {

        _gridView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                _gridView.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                View lastChild = _gridView.getChildAt(_gridView.getChildCount() - 1);
                if (lastChild != null)
                    _gridView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, lastChild.getBottom()));
                else
                    _gridView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,0));
            }
        });


    }

    /**
     * Forbids the user to order more , if hideMenu is true
     * @param menu
     * @return
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        setTitle(restaurantName);
        getMenuInflater().inflate(R.menu.main, menu);
        if (hideMenu == true)
            menu.getItem(0).setVisible(false);
        else
            menu.getItem(0).setVisible(true);
        return true;
    }

    /**
     * If send menu is selected, verify wich menu is selected and send order command (id) to server
     * @param item
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
            case R.id.menu_sendButton:
                SendMenuStandardModel sendMenuStandardModel = MenuListSingleton.getInstance().getSendMenuStandardModel();
                SendMenuFitnessModel sendMenuFitnessModel = new SendMenuFitnessModel();
                if (MenuListSingleton.getInstance().getStandardDish1_id() == null && MenuListSingleton.getInstance().getFitnessDish1_id() != null) {
                    sendMenuFitnessModel.setToken(UserSingleton.getInstance().getToken());
                    sendMenuFitnessModel.setDish1_id(MenuListSingleton.getInstance().getFitnessDish1_id());
                    RestClient.networkHandler().sendFitnessMenuId(sendMenuFitnessModel, new Callback<JSONObject>() {
                        @Override
                        public void success(JSONObject jsonObject, Response response) {
                            Toast.makeText(MenuListActivity.this, "Menu Fitness sent", Toast.LENGTH_SHORT).show();
                            finish();
                            startActivity(new Intent(MenuListActivity.this, MainFragmentActivity.class));
                        }

                        @Override
                        public void failure(RetrofitError error) {
                            Toast.makeText(MenuListActivity.this, "Failure send Menu Fitness " + error, Toast.LENGTH_SHORT).show();
                        }
                    });
                } else if (MenuListSingleton.getInstance().getFitnessDish1_id() == null && MenuListSingleton.getInstance().getStandardDish1_id() != null && MenuListSingleton.getInstance().getStandardDish2_id() != null && MenuListSingleton.getInstance().getStandardDessert_id() != null) {
                    sendMenuStandardModel.setToken(UserSingleton.getInstance().getToken());
                    RestClient.networkHandler().sendStandardMenuId(sendMenuStandardModel, new Callback<JSONObject>() {
                        @Override
                        public void success(JSONObject jsonObject, Response response) {
                            Toast.makeText(MenuListActivity.this, "Menu standard sent", Toast.LENGTH_SHORT).show();
                            finish();
                            startActivity(new Intent(MenuListActivity.this, MainFragmentActivity.class));
                        }

                        @Override
                        public void failure(RetrofitError error) {
                            Toast.makeText(MenuListActivity.this, "Failure send Menu Standard" + error, Toast.LENGTH_SHORT).show();
                        }
                    });
                } else
                    Toast.makeText(this, "Select a Standard or Fitness menu", Toast.LENGTH_SHORT).show();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
