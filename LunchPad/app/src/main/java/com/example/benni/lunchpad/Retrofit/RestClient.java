package com.example.benni.lunchpad.Retrofit;

import com.squareup.okhttp.OkHttpClient;

import java.util.concurrent.TimeUnit;

import retrofit.RestAdapter;
import retrofit.client.OkClient;

/**
 * Created bye BENNI on 9/13/2016.
 */
public class RestClient {

    private static API REST_CLIENT;
    private static  String EXTERNAL_ROOT = "http://192.168.151.43:8000/"; //http://192.168.151.43:8000/ http://lunch123.herokuapp.com/

    static {
        setupRestClient();
    }

    private RestClient(){
    }

    public static API networkHandler(){
        return REST_CLIENT;
    }

    private static void setupRestClient() {
        OkHttpClient okHttpClient = new OkHttpClient();
        okHttpClient.setReadTimeout(60, TimeUnit.SECONDS);
        RestAdapter.Builder builder = new RestAdapter.Builder()
                .setEndpoint(EXTERNAL_ROOT)
                .setClient(new OkClient(okHttpClient));
        builder.setLogLevel(RestAdapter.LogLevel.FULL);
        RestAdapter restAdapter = builder.build();
        REST_CLIENT = restAdapter.create(API.class);


    }
}
