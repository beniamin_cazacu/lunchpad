package com.example.benni.lunchpad.Models;

/**
 * Created bye BENNI on 9/15/2016.
 */
public class RatingModel {
    private String order_id;
    private Integer rating;
    private String token;

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}