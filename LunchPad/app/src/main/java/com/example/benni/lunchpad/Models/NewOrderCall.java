package com.example.benni.lunchpad.Models;

/**
 * Created bye BENNI on 9/15/2016.
 */
public class NewOrderCall {
    private Integer menu_id;
    private String token;

    public Integer getMenu_id() {
        return menu_id;
    }

    public void setMenu_id(Integer menu_id) {
        this.menu_id = menu_id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public NewOrderCall() {

    }

}
