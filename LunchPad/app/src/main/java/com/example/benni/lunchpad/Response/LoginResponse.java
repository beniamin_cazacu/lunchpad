package com.example.benni.lunchpad.Response;

/**
 * Created bye BENNI on 9/21/2016.
 */

public class LoginResponse {
    private String status;
    private String auth_token;

    public LoginResponse(String status, String auth_token){

        this.status = status;
        this.auth_token = auth_token;
    }


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAuth_token() {
        return auth_token;
    }

    public void setAuth_token(String auth_token) {
        this.auth_token = auth_token;
    }
}
