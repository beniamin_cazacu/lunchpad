package com.example.benni.lunchpad.Activity;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.benni.lunchpad.Fragments.UserProfileFragment;
import com.example.benni.lunchpad.Models.UserInformation;
import com.example.benni.lunchpad.R;
import com.example.benni.lunchpad.Retrofit.RestClient;
import com.example.benni.lunchpad.Singleton.UserSingleton;

import org.json.JSONObject;

import java.io.File;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedFile;


/**
 * Created bye BENNI on 9/20/2016.
 */
public class EditProfileActivity extends AppCompatActivity implements View.OnClickListener {

// just a test
    String picturePath;

    UserProfileFragment fragment;

    UserInformation userInformation;

    private ImageView image;
    private Bitmap bitmap;

    // number of images to select
    private static final int PICK_IMAGE = 1;


    Button button;

    ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_user);

        userInformation = new UserInformation();

        button = (Button) findViewById(R.id.activity_profile__editProfileButton);
        button.setOnClickListener(this);

        // Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        //  setSupportActionBar(toolbar);

        //  fragment = (UserProfileFragment)getSupportFragmentManager().findFragmentById(R.id.fragmentProfile) ;
        //  fragment.populateProfileFields();


//        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
//        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
//                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
//        drawer.setDrawerListener(toggle);
//        toggle.syncState();
//
//        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
//        navigationView.setNavigationItemSelectedListener(this);

        image = (ImageView) findViewById(R.id.image_profile);
        // Picasso.with(getApplicationContext()).load(new File("storage/emulated/0/Download/boot-1620452_1920.jpg")).into(image);

        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectImageFromGallery();
            }
        });
    }

//    @Override
//    public void onBackPressed() {
//        DrawerLayout drawer = (DrawerLayout)findViewById(R.id.drawer_layout);
//        if(drawer.isDrawerOpen(GravityCompat.START)){
//            drawer.closeDrawer(GravityCompat.START);
//        } else {
//            super.onBackPressed();
//        }
//    }


    /**
     * Opens dialog picker, so the user can select image from the gallery. The
     * result is returned in the method <code>onActivityResult()</code>
     */
    public void selectImageFromGallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select picture"), PICK_IMAGE);
    }

    /**
     * Retrives the result returned from selecting image, by invoking the method
     * <code>selectImageFromGallery()</code>
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE && resultCode == RESULT_OK && null != data) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = {
                    MediaStore.Images.Media.DATA
            };
            Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            picturePath = cursor.getString(columnIndex);
            Log.d("EDITPROFILE:", picturePath + ".................");
            cursor.close();

            decodeFile(picturePath);
        }
    }

    private String getRealPathFromURI(Uri contentURI) {
        String result;
        Cursor cursor = getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) { // Source is Dropbox or other similar local file path
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }

    /**
     * The method decodes the image file to avoid out of memory issues. Sets the
     * selected image in to the ImageView.
     *
     * @param filePath
     */
    public void decodeFile(String filePath) {
        // Decode image size
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(filePath, o);

        // The new size we want to scale to
        final int REQUIRED_SIZE = 1024;

        // Find the correct scale value . It should be the power of 2
        int width_tmp = o.outWidth, height_tmp = o.outHeight;


        int scale = 1;
        while (true) {
            if (width_tmp < REQUIRED_SIZE && height_tmp < REQUIRED_SIZE)
                break;
            width_tmp /= 2;
            height_tmp /= 2;
            scale *= 2;
        }

        // Decode with inSampleSize
        BitmapFactory.Options o2 = new BitmapFactory.Options();
        o2.inSampleSize = scale;
        bitmap = BitmapFactory.decodeFile(filePath, o2);

        image.setImageBitmap(bitmap);
    }

//    @SuppressWarnings("StatementWithEmptyBody")
//    @Override
//    public boolean onNavigationItemSelected(MenuItem item) {
//
//        // Handle navigation view item clicks here
//
//        DrawerLayout drawer = (DrawerLayout)findViewById(R.id.drawer_layout);
//        drawer.closeDrawer(GravityCompat.START);
//        return true;
//    }


    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.activity_profile__editProfileButton) {
            button.setEnabled(false);
            TypedFile typedFile = new TypedFile("image/png", new File(picturePath));

//            userInformation.setName("Test2 Beniamin");
//            userInformation.setPhone("12345678902");
//            userInformation.setAddress("Test Suceava2");
//            userInformation.setToken("-CQ5BoVzV4YuG3ywSqgD");
//            Log.e("TOKEN:::", userInformation.getToken());
            RestClient.networkHandler().sendDataProfile(typedFile, userInformation.getName(), userInformation.getPhone(), userInformation.getAddress(), UserSingleton.getInstance().getToken(), new Callback<JSONObject>() {
                @Override
                public void success(JSONObject s, Response response) {
                    Log.d("UPLOAD: ", "SUCCESS.." + response);
                    Toast.makeText(getApplicationContext(), "SUCCES " + response, Toast.LENGTH_SHORT).show();
                    // startActivity(new Intent(EditProfileActivity.this,MenuListActivity.class));
                }

                @Override
                public void failure(RetrofitError error) {
                    Toast.makeText(getApplicationContext(), "FAILURE " + error, Toast.LENGTH_SHORT).show();
                    Log.d("UPLOAD: ", "FAIL.." + error);
                    button.setEnabled(true);
                }
            });
            RestClient.networkHandler().getUserProfile(UserSingleton.getInstance().getToken(), new Callback<UserInformation>() {
                @Override
                public void success(UserInformation userInformation, Response response) {
                    UserSingleton.getInstance().setUserInformation(userInformation);
                    Toast.makeText(getApplicationContext(), "SUCCES PROFILE " + response, Toast.LENGTH_SHORT).show();
                }

                @Override
                public void failure(RetrofitError error) {
                    Toast.makeText(getApplicationContext(), "ERROR GET USER DATA" + error, Toast.LENGTH_SHORT).show();
                    error.printStackTrace();
                    button.setEnabled(true);
                }
            });


        }

    }

}
