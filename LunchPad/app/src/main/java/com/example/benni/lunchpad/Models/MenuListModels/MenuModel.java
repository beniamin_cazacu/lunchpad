package com.example.benni.lunchpad.Models.MenuListModels;

/**
 * Created bye BENNI on 9/27/2016.
 * Model for a menu
 */

public class MenuModel {
    private Integer id;
    private String title;
    private String picture_url;

    public MenuModel(){}

    public MenuModel(Integer id, String title, String picture_url) {
        this.id = id;
        this.title = title;
        this.picture_url = picture_url;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPicture_url() {
        return picture_url;
    }

    public void setPicture_url(String picture_url) {
        this.picture_url = picture_url;
    }
}
