package com.example.benni.lunchpad.Models.MenuListModels;

import android.view.ViewGroup;

import java.util.List;

/**
 * Created bye BENNI on 9/29/2016.
 */

public class ViewGroupsMenuModel {
    private List<ViewGroup> viewGroupList;

    public ViewGroupsMenuModel(){}

    public ViewGroupsMenuModel(List<ViewGroup> viewGroupList) {
        this.viewGroupList = viewGroupList;
    }

    public List<ViewGroup> getViewGroupList() {
        return viewGroupList;
    }

    public void setViewGroupList(List<ViewGroup> viewGroupList) {
        this.viewGroupList = viewGroupList;
    }

    public void addViewGroup(ViewGroup viewGroup){
        viewGroupList.add(viewGroup);
    }
    public Integer sizeViewGroup(){
        return viewGroupList.size();
    }
    public boolean contains(ViewGroup viewGroup){
        return viewGroupList.contains(viewGroup);
    }
    public boolean access(int index){
        if (viewGroupList.get(index) == null) {
            return false;
        } else
            return true;
    }
}
