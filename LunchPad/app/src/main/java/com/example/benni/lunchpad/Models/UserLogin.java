package com.example.benni.lunchpad.Models;

/**
 * Created bye BENNI on 9/14/2016.
 * Used to send login information
 */
public class UserLogin {
    private String email;
    private String password;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
