package com.example.benni.lunchpad.Response.MenusList;

/**
 * Created bye BENNI on 9/27/2016.
 */

public class MenuDishesResponse {
    private Integer id;
    private String dish_title;
    private String dish_type;
    private String picture_url;

    public MenuDishesResponse(){}

    public MenuDishesResponse(Integer id, String dish_title, String dish_type, String picture_url) {
        this.id = id;
        this.dish_title = dish_title;
        this.dish_type = dish_type;
        this.picture_url = picture_url;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDish_title() {
        return dish_title;
    }

    public void setDish_title(String dish_title) {
        this.dish_title = dish_title;
    }

    public String getDish_type() {
        return dish_type;
    }

    public void setDish_type(String dish_type) {
        this.dish_type = dish_type;
    }

    public String getPicture_url() {
        return picture_url;
    }

    public void setPicture_url(String picture_url) {
        this.picture_url = picture_url;
    }
}
