package com.example.benni.lunchpad.Adapters;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.benni.lunchpad.Activity.MenuListActivity;
import com.example.benni.lunchpad.Models.MenuListModels.OrderViewModel;
import com.example.benni.lunchpad.R;
import com.example.benni.lunchpad.Singleton.MenuListSingleton;

import java.util.ArrayList;
import java.util.List;

/**
 * Created bye BENNI on 9/29/2016.
 */

public class RecycleAdapter extends RecyclerView.Adapter<RecycleAdapter.RecycleViewHolder> {
    private List<OrderViewModel> orderViewList = new ArrayList<>(); // keep the list of all orders date and status

    public RecycleAdapter(List<OrderViewModel> orderViewList) {
        this.orderViewList = orderViewList;
    }
    /**
     * Create new views (invoked by the layout manager)
     * @param parent
     * @param viewType
     * @return recycleViewHolder
     */
    @Override
    public RecycleViewHolder onCreateViewHolder(final ViewGroup parent, int viewType) {
        // create a new View
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_order_view, parent, false);
        // instance of RecycleViewHolder class
        final RecycleViewHolder recycleViewHolder = new RecycleViewHolder(view);
        // if user clicks on the view, start MenuListActivity depending the status of menu
        // if is OPEN - user can make order, else not


       // view.setBackgroundColor(Color.parseColor("#000000"));
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int itemPosition = recycleViewHolder.getPosition();
                String item = orderViewList.get(itemPosition).getMenu_status();
                MenuListSingleton.getInstance().setMenu_date(orderViewList.get(itemPosition).getDate());
                if (item.equals("OPEN")) {
                    MenuListSingleton.getInstance().setMenu_status("OPEN");
                    Intent intent = new Intent(v.getContext(), MenuListActivity.class);
                    v.getContext().startActivity(intent);
                } else {
                    MenuListSingleton.getInstance().setMenu_status("CLOSED");
                    Intent intent = new Intent(v.getContext(), MenuListActivity.class);
                    v.getContext().startActivity(intent);
                }
                Toast.makeText(v.getContext(), item, Toast.LENGTH_SHORT).show();
            }
        });

        return recycleViewHolder;
    }
    /**
     * Replace the contents of a view (invoked by the layout manager)
     * @param holder - instance of RecycleViewHolder
     * @param position - the position
     */
    @Override
    public void onBindViewHolder(RecycleViewHolder holder, int position) {
        OrderViewModel dataProvider = orderViewList.get(position);
        holder.orderViewData.setText(dataProvider.getDate());
        holder.orderViewState.setText(dataProvider.getMenu_status());
    }

    /**
     * @return the size of the list
     */
    @Override
    public int getItemCount() {
        return orderViewList.size();
    }

    /**
     * Provide a reference to the views for each data item
     */
    public static class RecycleViewHolder extends RecyclerView.ViewHolder {

        TextView orderViewData, orderViewState;

        public RecycleViewHolder(View itemView) {
            super(itemView);
            orderViewData = (TextView) itemView.findViewById(R.id.order_view_data);
            orderViewState = (TextView) itemView.findViewById(R.id.order_view_state);
        }
    }
}
