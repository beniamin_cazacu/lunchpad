package com.example.benni.lunchpad.Models.LoginModels;

/**
 * Created bye BENNI on 9/21/2016.
 */

public class UserLoginFacebook {
    private String name;
    private String email;

    public UserLoginFacebook() {
    }

    public UserLoginFacebook(String name, String email) {
        this.name = name;
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
