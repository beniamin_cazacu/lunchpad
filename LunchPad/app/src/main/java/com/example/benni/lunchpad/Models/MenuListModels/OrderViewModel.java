package com.example.benni.lunchpad.Models.MenuListModels;

/**
 * Created bye BENNI on 9/29/2016.
 * Model for history orders
 */

public class OrderViewModel {
    private String date;
    private String menu_status;

    public OrderViewModel(){}

    public OrderViewModel(String date, String menu_status) {
        this.date = date;
        this.menu_status = menu_status;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getMenu_status() {
        return menu_status;
    }

    public void setMenu_status(String menu_status) {
        this.menu_status = menu_status;
    }
}
