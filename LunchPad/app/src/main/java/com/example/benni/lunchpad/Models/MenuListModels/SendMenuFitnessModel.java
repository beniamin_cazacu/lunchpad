package com.example.benni.lunchpad.Models.MenuListModels;

/**
 * Created bye BENNI on 9/29/2016.
 * Used to send Fitness menu
 */

public class SendMenuFitnessModel {
    private String token;
    private Integer dish1_id;

    public SendMenuFitnessModel(){}

    public SendMenuFitnessModel(String token, Integer dish1_id) {
        this.token = token;
        this.dish1_id = dish1_id;
    }

    public Integer getDish1_id() {
        return dish1_id;
    }

    public void setDish1_id(Integer dish1_id) {
        this.dish1_id = dish1_id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
