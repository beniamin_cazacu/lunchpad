package com.example.benni.lunchpad.Tests;

/**
 * Created bye BENNI on 9/26/2016.
 */

public class MenusModel {
    private String title;
    private String restaurant_name;
    private String first_dish;
    private String picture_first_dish;
    private String second_dish;
    private String picture_second_dish;
    private String dessert;
    private String picture_dessert;

    public MenusModel(){}

    public MenusModel(String title, String restaurant_name, String first_dish, String picture_first_dish, String second_dish, String picture_second_dish, String dessert, String picture_dessert) {
        this.title = title;
        this.restaurant_name = restaurant_name;
        this.first_dish = first_dish;
        this.picture_first_dish = picture_first_dish;
        this.second_dish = second_dish;
        this.picture_second_dish = picture_second_dish;
        this.dessert = dessert;
        this.picture_dessert = picture_dessert;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getRestaurant_name() {
        return restaurant_name;
    }

    public void setRestaurant_name(String restaurant_name) {
        this.restaurant_name = restaurant_name;
    }

    public String getFirst_dish() {
        return first_dish;
    }

    public void setFirst_dish(String first_dish) {
        this.first_dish = first_dish;
    }

    public String getPicture_first_dish() {
        return picture_first_dish;
    }

    public void setPicture_first_dish(String picture_first_dish) {
        this.picture_first_dish = picture_first_dish;
    }

    public String getSecond_dish() {
        return second_dish;
    }

    public void setSecond_dish(String second_dish) {
        this.second_dish = second_dish;
    }

    public String getPicture_second_dish() {
        return picture_second_dish;
    }

    public void setPicture_second_dish(String picture_second_dish) {
        this.picture_second_dish = picture_second_dish;
    }

    public String getDessert() {
        return dessert;
    }

    public void setDessert(String dessert) {
        this.dessert = dessert;
    }

    public String getPicture_dessert() {
        return picture_dessert;
    }

    public void setPicture_dessert(String picture_dessert) {
        this.picture_dessert = picture_dessert;
    }
}
