package com.example.benni.lunchpad.Retrofit;

import com.example.benni.lunchpad.Models.DeviceIDModel;
import com.example.benni.lunchpad.Models.MenuListModels.OrderViewModel;
import com.example.benni.lunchpad.Models.MenuListModels.SendMenuFitnessModel;
import com.example.benni.lunchpad.Models.MenuListModels.SendMenuStandardModel;
import com.example.benni.lunchpad.Models.MenuListModels.SendRatingModel;
import com.example.benni.lunchpad.Models.UserInformation;
import com.example.benni.lunchpad.Models.UserLogin;
import com.example.benni.lunchpad.Response.LoginResponse;
import com.example.benni.lunchpad.Response.MenusList.CodeMenuResponse;
import com.example.benni.lunchpad.Response.MenusList.MenuListResponse;
import com.example.benni.lunchpad.Response.MenusList.OrderHistoryResponse;

import org.json.JSONObject;

import java.util.List;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Part;
import retrofit.http.Query;
import retrofit.mime.TypedFile;

/**
 * Created bye BENNI on 9/13/2016.
 */
public interface API {

    @POST("/api/v1/users/log_in")
    void sendDataLogin(@Body UserLogin userLogin ,
                       Callback<LoginResponse> Response);

    @POST("/api/v1/users/sign_up")
    void sendDataRegister(@Body UserInformation userInformation ,
                          Callback <JSONObject> Response);

    @GET("/api/v1/users/profile")
    void getUserProfile(@Query("token") String token,
                        Callback<UserInformation> callback);

    @Multipart
    @PUT("/api/v1/users/edit")
    void sendDataProfile(@Part("avatar") TypedFile avatar,
                         @Part("name") String name,
                         @Part("phone") String phone,
                         @Part("address") String address,
                         @Part("token")String token,
                         Callback<JSONObject> response);

    @GET("/api/v1/menus")
    void getMenuList(@Query("token") String token,
                     @Query("date") String date,
                     Callback<List<MenuListResponse>> callback);


    @POST("/api/v1/orders/new")
    void sendStandardMenuId(@Body SendMenuStandardModel sendMenuStandardModel,
                            Callback<JSONObject> response);

    @POST("/api/v1/orders/new")
    void sendFitnessMenuId(@Body SendMenuFitnessModel sendMenuFitnessModel,
                           Callback<JSONObject> response);

    @GET("/api/v1/menus/history")
    void getOrderHistory(Callback<List<OrderViewModel>> response);

    @GET("/api/v1/orders/check_code")
    void sendMenuCode(@Query("order_code") String order_code,
                      Callback <CodeMenuResponse> response);

    @POST("/api/v1/orders/rating")
    void sendMenuRating(@Body SendRatingModel sendRatingModel,
                        Callback <JSONObject> response);

    @GET("/api/v1/orders/history")
    void getOrderUsersHistory(@Query("date") String date,
                         Callback<List<OrderHistoryResponse>> response) ;

    @POST("/api/v1/users/device")
    void sendDeviceId(@Body DeviceIDModel deviceIDModel,
                      Callback<JSONObject> response);


}
