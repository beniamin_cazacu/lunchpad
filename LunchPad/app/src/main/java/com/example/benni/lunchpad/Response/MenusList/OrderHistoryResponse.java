package com.example.benni.lunchpad.Response.MenusList;

/**
 * Created bye BENNI on 10/3/2016.
 */

public class OrderHistoryResponse {
    private String  dish1_title;
    private String  dish2_title;
    private String  dessert_title;
    private UserOrderHistoryResponse user;

    public OrderHistoryResponse(){}
    public OrderHistoryResponse(String dish1_title, String dish2_title, String dessert_title, UserOrderHistoryResponse user) {
        this.dish1_title = dish1_title;
        this.dish2_title = dish2_title;
        this.dessert_title = dessert_title;
        this.user = user;
    }

    public String getDish1_title() {
        return dish1_title;
    }

    public void setDish1_title(String dish1_title) {
        this.dish1_title = dish1_title;
    }

    public String getDish2_title() {
        return dish2_title;
    }

    public void setDish2_title(String dish2_title) {
        this.dish2_title = dish2_title;
    }

    public String getDessert_title() {
        return dessert_title;
    }

    public void setDessert_title(String dessert_title) {
        this.dessert_title = dessert_title;
    }

    public UserOrderHistoryResponse getUser() {
        return user;
    }

    public void setUser(UserOrderHistoryResponse user) {
        this.user = user;
    }
}
