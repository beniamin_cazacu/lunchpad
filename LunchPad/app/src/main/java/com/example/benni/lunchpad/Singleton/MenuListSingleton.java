package com.example.benni.lunchpad.Singleton;

import com.example.benni.lunchpad.Models.MenuListModels.SendMenuFitnessModel;
import com.example.benni.lunchpad.Models.MenuListModels.SendMenuStandardModel;
import com.example.benni.lunchpad.Response.MenusList.CodeMenuResponse;

/**
 * Created bye BENNI on 9/29/2016.
 */
public class MenuListSingleton {

    private String menu_status = "OPEN"; // Open or Closed
    private String menu_date = "0/0/0";

    private SendMenuStandardModel sendMenuStandardModel = new SendMenuStandardModel();
    private SendMenuFitnessModel sendMenuFitnessModel = new SendMenuFitnessModel();
    private CodeMenuResponse codeMenuResponse = new CodeMenuResponse();

    private static MenuListSingleton ourInstance = new MenuListSingleton();

    public static MenuListSingleton getInstance() {
        return ourInstance;
    }

    private MenuListSingleton() {
    }

    public SendMenuStandardModel getSendMenuStandardModel(){
        return sendMenuStandardModel;
    }
    public void setSendMenuStandardModel(SendMenuStandardModel menuModel){
        this.sendMenuStandardModel = menuModel;
    }


    public Integer getStandardDish1_id() {
        return sendMenuStandardModel.getDish1_id();
    }

    public void setStandardDish1_id(Integer dish1_id) {
        sendMenuStandardModel.setDish1_id(dish1_id);
    }

    public Integer getStandardDish2_id() {
        return sendMenuStandardModel.getDish2_id();
    }

    public void setStandardDish2_id(Integer dish2_id) {
        sendMenuStandardModel.setDish2_id(dish2_id);
    }

    public Integer getStandardDessert_id() {
        return sendMenuStandardModel.getDessert_id();
    }

    public void setStandardDish3_id(Integer dish3_id) {
        sendMenuStandardModel.setDessert_id(dish3_id);
    }

    public Integer getFitnessDish1_id() {
        return sendMenuFitnessModel.getDish1_id();
    }

    public void setFitnessDish1_id(Integer dish1_id) {
        sendMenuFitnessModel.setDish1_id(dish1_id);
    }

    public String getMenu_status() {
        return menu_status;
    }

    public void setMenu_status(String menu_status) {
        this.menu_status = menu_status;
    }

    public CodeMenuResponse getCodeMenuResponse() {
        return codeMenuResponse;
    }

    public void setCodeMenuResponse(CodeMenuResponse codeMenuResponse) {
        this.codeMenuResponse = codeMenuResponse;
    }

    public String getMenu_date() {
        return menu_date;
    }

    public void setMenu_date(String menu_date) {
        this.menu_date = menu_date;
    }
}
