package com.example.benni.lunchpad.Adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.benni.lunchpad.Models.MenuListModels.MenuModel;
import com.example.benni.lunchpad.Models.MenuListModels.SendMenuFitnessModel;
import com.example.benni.lunchpad.Models.MenuListModels.SendMenuStandardModel;
import com.example.benni.lunchpad.Models.MenuListModels.ViewGroupsMenuModel;
import com.example.benni.lunchpad.R;
import com.example.benni.lunchpad.Singleton.MenuListSingleton;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created bye BENNI on 9/26/2016.
 */

public class MenuAdapter extends BaseAdapter {

    /**
     * Keep a list of viewgroup , used when user has clicked a menu.
     * If user click a menu , it is passed in a list, to be able to access on those information later
     */
    private static ViewGroupsMenuModel viewGroupList = new ViewGroupsMenuModel(new ArrayList<ViewGroup>());

    private SendMenuStandardModel sendMenuStandardModel = new SendMenuStandardModel();
    private SendMenuFitnessModel sendMenuFitnessModel = new SendMenuFitnessModel();

    private List<MenuModel> mMenuList; // keep all information about menus

    private Context mContext;

    /**
     * Initializes context and menuList
     *
     * @param c
     * @param mMenuList
     */
    public MenuAdapter(Context c, List<MenuModel> mMenuList) {
        mContext = c;
        this.mMenuList = mMenuList;
    }

    /**
     * Size of list
     *
     * @return
     */
    public int getCount() {
        return mMenuList.size();
    }

    /**
     * @param position
     * @return item from that position
     */
    public Object getItem(int position) {
        return mMenuList.get(position);
    }

    /**
     * @param position
     * @return position element
     */
    public long getItemId(int position) {
        return position;
    }

    /**
     * Check if a menu has been selected
     * If has selected, unselect the other menus
     * User can do a order only once
     * Send order id to server
     * Displays all the users who have placed an order
     * @param position - position of element
     * @param convertView - the view
     * @param parent - the ViewGroup
     * @return the view
     */
    public View getView(final int position, final View convertView, final ViewGroup parent) {

         final View view = View.inflate(mContext, R.layout.menu_list_model, null);
        final ImageView imageChecked;

        TextView menuTitle = (TextView) view.findViewById(R.id.menu_list_menuName);
        ImageView imageMenu = (ImageView) view.findViewById(R.id.menu_list_menuImage);
        imageChecked = (ImageView) view.findViewById(R.id.menu_list_checkMenuImage); // used to check or uncheck

        // if status is OPEN , user can make an order
        if (MenuListSingleton.getInstance().getMenu_status().equals("OPEN")) {
            // if user clicks on a view , check if a view is checked - imageChecked is visible , and make it invisible
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // if imageCheck is visible, make it invisible - uncheck it
                    if (imageChecked.getVisibility() == View.VISIBLE) {
                        imageChecked.setVisibility(View.INVISIBLE);
                        sendMenuStandardModel.setDish1_id(null);
                        String viewTag = parent.getTag().toString(); // keep the type of menu
                        // make all menus null
                        switch (viewTag) {
                            case "First Dish":
                                MenuListSingleton.getInstance().setStandardDish1_id(null);
                                MenuListSingleton.getInstance().setFitnessDish1_id(null);
                                break;
                            case "Second Dish":
                                MenuListSingleton.getInstance().setStandardDish2_id(null);
                                MenuListSingleton.getInstance().setFitnessDish1_id(null);
                                break;
                            case "Dessert":
                                MenuListSingleton.getInstance().setStandardDish3_id(null);
                                MenuListSingleton.getInstance().setFitnessDish1_id(null);
                                break;
                            case "Fitness":
                                MenuListSingleton.getInstance().setFitnessDish1_id(null);
                            default:
                                Toast.makeText(mContext, "NOT", Toast.LENGTH_SHORT).show();
                        }
                        MenuListSingleton.getInstance().setStandardDish1_id(null);
                    } else {
                        imageChecked.setVisibility(View.VISIBLE);
                    }
                    // ensure that the list contains only a ViewGroup
                    if (!viewGroupList.contains(parent)) {
                        viewGroupList.addViewGroup(parent);
                    }
                    // Access childs of parent ViewGroup
                    // Acces image for that child
                    for (int i = 0; i < parent.getChildCount(); i++) {
                        View view1 = parent.getChildAt(i);
                        ImageView imageView;
                        imageView = (ImageView) view1.findViewById(R.id.menu_list_checkMenuImage);
                        // if is visible, make it invisible, except image for that position
                        if (imageView.getVisibility() == View.VISIBLE) {
                            if (position != i) {
                                if (imageChecked.getVisibility() == View.VISIBLE) {
                                    imageView.setVisibility(View.INVISIBLE);
                                    // verify tags, if it's Standard menu , deselect Fitness menu
                                }
                            }
                            checkTags(parent, position);
                        }
                    }
                }
            });
        }
        menuTitle.setText(mMenuList.get(position).getTitle());
        // load with image
        Picasso.with(mContext)
                .load(mMenuList.get(position).getPicture_url())
                .error(R.mipmap.profile2)
                .resize(200,200)
                .into(imageMenu);
        return view;
    }

    /**
     * Identify the type of menu and set the ID's
     * @param _viewGroup - the ViewGroup
     * @param _position - position of element
     */
    private void checkTags(ViewGroup _viewGroup, Integer _position) {
        // check the type of standard menu
        if (_viewGroup.getTag().equals("First Dish") || _viewGroup.getTag().equals("Second Dish") || _viewGroup.getTag().equals("Dessert")) {
            String viewTag = _viewGroup.getTag().toString(); // keep the type of menu and, depending on the menu , set the id
            switch (viewTag) {
                case "First Dish":
                    MenuListSingleton.getInstance().setStandardDish1_id(mMenuList.get(_position).getId());
                    MenuListSingleton.getInstance().setFitnessDish1_id(null);
                    break;
                case "Second Dish":
                    MenuListSingleton.getInstance().setStandardDish2_id(mMenuList.get(_position).getId());
                    MenuListSingleton.getInstance().setFitnessDish1_id(null);
                    break;
                case "Dessert":
                    MenuListSingleton.getInstance().setStandardDish3_id(mMenuList.get(_position).getId());
                    MenuListSingleton.getInstance().setFitnessDish1_id(null);
                    break;
                default:
                    Toast.makeText(mContext, "ANOTHER TYPE OF MENU", Toast.LENGTH_SHORT).show();
            }

            for (int j = 0; j < viewGroupList.sizeViewGroup(); j++) {
                // if accessed Fitness GroupView, deselect it = make image invisible
                if (viewGroupList.getViewGroupList().get(j).getTag().equals("Fitness")) {
                    for (int k = 0; k < viewGroupList.getViewGroupList().get(j).getChildCount(); k++) {
                        View view2 = viewGroupList.getViewGroupList().get(j).getChildAt(k);
                        ImageView imageView2;
                        imageView2 = (ImageView) view2.findViewById(R.id.menu_list_checkMenuImage);
                        imageView2.setVisibility(View.INVISIBLE);
                    }
                }
            }
        }
        if (_viewGroup.getTag().equals("Fitness")) {
            // if is fitness, set it ID and make the other ID's null
            MenuListSingleton.getInstance().setFitnessDish1_id(mMenuList.get(_position).getId());

            MenuListSingleton.getInstance().setStandardDish1_id(null);

            MenuListSingleton.getInstance().setStandardDish2_id(null);

            MenuListSingleton.getInstance().setStandardDish3_id(null);

            for (int j = 0; j < viewGroupList.sizeViewGroup(); j++) {
                // if accessed Fitness GroupView, deselect it = make image invisible
                if (viewGroupList.getViewGroupList().get(j).getTag().equals("First Dish") || viewGroupList.getViewGroupList().get(j).getTag().equals("Second Dish") || viewGroupList.getViewGroupList().get(j).getTag().equals("Dessert")) {
                    for (int k = 0; k < viewGroupList.getViewGroupList().get(j).getChildCount(); k++) {
                        View view2 = viewGroupList.getViewGroupList().get(j).getChildAt(k);
                        ImageView imageView2;
                        imageView2 = (ImageView) view2.findViewById(R.id.menu_list_checkMenuImage);
                        imageView2.setVisibility(View.INVISIBLE);
                    }
                }
            }
        }
    }
}
