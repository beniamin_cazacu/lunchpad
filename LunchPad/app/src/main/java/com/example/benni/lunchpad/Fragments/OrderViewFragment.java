package com.example.benni.lunchpad.Fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.benni.lunchpad.Adapters.RecycleAdapter;
import com.example.benni.lunchpad.Models.MenuListModels.OrderViewModel;
import com.example.benni.lunchpad.R;
import com.example.benni.lunchpad.Retrofit.RestClient;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OrderViewFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link OrderViewFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class OrderViewFragment extends Fragment {

    RecyclerView recyclerView;
    RecyclerView.Adapter adapter;
    RecyclerView.LayoutManager layoutManager;

    List<OrderViewModel> orderViewModelList = new ArrayList<>(); // keep a list with orderView information

    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";

    private String mParam1;

    private OnFragmentInteractionListener mListener;

    public OrderViewFragment() {
        // Required empty public constructor
    }
    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     * @param param1 Parameter 1.
     * @return A new instance of fragment OrderViewFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static OrderViewFragment newInstance(String param1) {
        OrderViewFragment fragment = new OrderViewFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
        }
    }

    /**
     * Inflate the layout
     * @param inflater - LayoutInflater
     * @param container -ViewGroup
     * @param savedInstanceState - the bundle
     * @return layout inflated
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_order_view, container, false);
    }

    /**
     *  Get order History from server
     *  Add information into list , check if date isn't twice
     *  Set the adapter
     * @param view - view
     * @param savedInstanceState - bundle
     */
    @Override
    public void onViewCreated(final View view, Bundle savedInstanceState) {
        recyclerView = (RecyclerView) view.findViewById(R.id.recycle_view);
        RestClient.networkHandler().getOrderHistory(new Callback<List<OrderViewModel>>() {
            @Override
            public void success(List<OrderViewModel> orderViewModel, Response response) {
                orderViewModelList.add(new OrderViewModel(orderViewModel.get(0).getDate(), orderViewModel.get(0).getMenu_status()));
                for (int i = 0; i < orderViewModel.size() - 1; i++) {
                    if (!orderViewModel.get(i).getDate().equals(orderViewModel.get(i + 1).getDate()))
                        orderViewModelList.add(new OrderViewModel(orderViewModel.get(i).getDate(), orderViewModel.get(i).getMenu_status()));

                    adapter = new RecycleAdapter(orderViewModelList);
                    // use this setting to improve performance if you know that changes
                    // in content do not change the layout size of the RecyclerView
                    recyclerView.setHasFixedSize(true);
                    // use a linear layout manager
                    layoutManager = new LinearLayoutManager(view.getContext());
                    recyclerView.setLayoutManager(layoutManager);
                    // specify an adapter
                    recyclerView.setAdapter(adapter);
                }
            }
            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(view.getContext(), "FAILURE ORDER HISTORY " + error, Toast.LENGTH_SHORT).show();

            }
        });

    }
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }
    /**
     * Called when fragment is attached with activity
     * @param context - activity context
     */
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }
    /**
     * Called when fragment is unlinked from the activity
     */
    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
