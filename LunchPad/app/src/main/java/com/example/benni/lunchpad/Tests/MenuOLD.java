package com.example.benni.lunchpad.Tests;

import android.support.annotation.NonNull;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

/**
 * Created bye BENNI on 9/15/2016.
 */
public class MenuOLD {
    private String title;
    private String firstDish;
    private String secondDish;
    private String dessert;
    private Integer id;
    private List<Integer> arrID = new List<Integer>(){

        @Override
        public int size() {
            return 0;
        }

        @Override
        public boolean isEmpty() {
            return false;
        }

        @Override
        public boolean contains(Object o) {
            return false;
        }

        @NonNull
        @Override
        public Iterator<Integer> iterator() {
            return null;
        }

        @NonNull
        @Override
        public Object[] toArray() {
            return new Object[0];
        }

        @NonNull
        @Override
        public <T> T[] toArray(T[] ts) {
            return null;
        }

        @Override
        public boolean add(Integer integer) {
            return false;
        }

        @Override
        public boolean remove(Object o) {
            return false;
        }

        @Override
        public boolean containsAll(Collection<?> collection) {
            return false;
        }

        @Override
        public boolean addAll(Collection<? extends Integer> collection) {
            return false;
        }

        @Override
        public boolean addAll(int i, Collection<? extends Integer> collection) {
            return false;
        }

        @Override
        public boolean removeAll(Collection<?> collection) {
            return false;
        }

        @Override
        public boolean retainAll(Collection<?> collection) {
            return false;
        }

        @Override
        public void clear() {

        }

        @Override
        public Integer get(int i) {
            return null;
        }

        @Override
        public Integer set(int i, Integer integer) {
            return null;
        }

        @Override
        public void add(int i, Integer integer) {

        }

        @Override
        public Integer remove(int i) {
            return null;
        }

        @Override
        public int indexOf(Object o) {
            return 0;
        }

        @Override
        public int lastIndexOf(Object o) {
            return 0;
        }

        @Override
        public ListIterator<Integer> listIterator() {
            return null;
        }

        @NonNull
        @Override
        public ListIterator<Integer> listIterator(int i) {
            return null;
        }

        @NonNull
        @Override
        public List<Integer> subList(int i, int i1) {
            return null;
        }
    };

    public MenuOLD(Integer id, String title, String firstDish, String secondDish, String dessert) {
        this.title = title;
        this.firstDish = firstDish;
        this.secondDish = secondDish;
        this.dessert = dessert;
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getFirstDish() {
        return firstDish;
    }

    public void setFirstDish(String firstDish) {
        this.firstDish = firstDish;
    }

    public String getSecondDish() {
        return secondDish;
    }

    public void setSecondDish(String secondDish) {
        this.secondDish = secondDish;
    }

    public String getDessert() {
        return dessert;
    }

    public void setDessert(String dessert) {
        this.dessert = dessert;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<Integer> getArrID() {
        return arrID;
    }

    public void setArrID(List<Integer> arrID) {
        this.arrID = arrID;
    }
}
