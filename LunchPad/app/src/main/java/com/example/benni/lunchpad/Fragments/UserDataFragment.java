package com.example.benni.lunchpad.Fragments;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.benni.lunchpad.Activity.MainFragmentActivity;
import com.example.benni.lunchpad.Models.UserInformation;
import com.example.benni.lunchpad.Models.UserLogin;
import com.example.benni.lunchpad.R;
import com.example.benni.lunchpad.Response.LoginResponse;
import com.example.benni.lunchpad.Retrofit.RestClient;
import com.example.benni.lunchpad.Singleton.UserSingleton;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.io.File;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedFile;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link UserDataFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link UserDataFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class UserDataFragment extends Fragment {
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    // parameters for facebook login
    private static final String ARG_PARAM2 = "param2"; // name
    private static final String ARG_PARAM3 = "param3"; // email


    private OnFragmentInteractionListener callback;
    // path of image
    public static String filePath ;

    UserInformation userInformation;
    UserLogin userLogin;

    TextInputEditText name;
    TextInputEditText email;
    TextInputEditText password;
    TextInputEditText password_confirmation;
    TextInputEditText phone_number;
    TextInputEditText address;
    Button sendButton;
    ImageView image;
    TextInputLayout textInputPassword, textInputPasswordConfirmation;
    static ImageView imageView; // used on user clicks on image


    private String mParam1; // fragment name
    private String mParam2; // name
    private String mParam3; // email

    private OnFragmentInteractionListener mListener;

    public UserDataFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1
     * @return A new instance of fragment RegistrationFragment.
     */
    public static UserDataFragment newInstance(String param1) {
        UserDataFragment fragment = new UserDataFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }
    // called for registration with facebook
    public static UserDataFragment newInstance(String param1,String param2,String param3) {
        UserDataFragment fragment = new UserDataFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        args.putString(ARG_PARAM3, param3);
        fragment.setArguments(args);
        return fragment;
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
            mParam3 = getArguments().getString(ARG_PARAM3);

        }
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_user_detail, container, false);
    }
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        userInformation = new UserInformation();
        userLogin = new UserLogin();
        textInputPassword = (TextInputLayout) view.findViewById(R.id.textInputLayout_password);
        textInputPasswordConfirmation = (TextInputLayout) view.findViewById(R.id.textInputLayout_password_confirmation);
        filePath = UserSingleton.getInstance().getUserInformation().getAvatar(); // path of image

        name = (TextInputEditText) view.findViewById(R.id.name);
        email = (TextInputEditText) view.findViewById(R.id.email);
        password = (TextInputEditText) view.findViewById(R.id.password);
        password_confirmation = (TextInputEditText) view.findViewById(R.id.password_confirmation);
        phone_number = (TextInputEditText) view.findViewById(R.id.phoneNumber);
        address = (TextInputEditText) view.findViewById(R.id.address);
        image = (ImageView) view.findViewById(R.id.image_profile);
        imageView = (ImageView) view.findViewById(R.id.image_profile);
        sendButton = (Button) view.findViewById(R.id.registration_editProfile_activity_signUpButton);
        // check text button to if need to register of edit profile
        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (sendButton.getText().toString().equals(getString(R.string.register_signUp))) {
                    RegistrationClicked();
                } else {
                    if (ValidationProfile()) {
                        updateDataProfile();
                    }
                }
            }
        });
        //
        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (callback != null) {
                    // call methods to change image on user click on image
                    callback.onUserDataFragmentInteraction(view);
                }
            }
        });
        // make settings for profile user
        if (mParam1.equals("profile")) {
            fillProfileFieldData();
            settingsForProfileView();
        }
        // make settings for user registration
        if (mParam1.equals("registration")) {
            settingsForRegistrationView();
        }
        // update data with facebook information
        if (mParam1.equals("facebookRegistration")) {
            settingsForRegistrationView();
            populateWithFacebookData(mParam2,mParam3);
        }
    }

    /**
     * Update user profile : name, phone number, address and token
     */
    private void updateDataProfile() {
        // get data form fields
        String strName = name.getText().toString();
        String strPhone = phone_number.getText().toString();
        String strAddress = address.getText().toString();
        final String strToken = UserSingleton.getInstance().getToken();

        if (filePath.contains("http"))
            Toast.makeText(getContext(), "SELECT A PICTURE PROFILE", Toast.LENGTH_SHORT).show();
        else {
            TypedFile typedFile = new TypedFile("image/png", new File(filePath));
            RestClient.networkHandler().sendDataProfile(typedFile, strName, strPhone, strAddress, strToken, new Callback<JSONObject>() {
                @Override
                public void success(JSONObject s, Response response) {
                    RestClient.networkHandler().getUserProfile(UserSingleton.getInstance().getToken(), new Callback<UserInformation>() {
                        @Override
                        public void success(UserInformation userInformation, Response response) {
                            UserSingleton.getInstance().setUserInformation(userInformation);
                            UserSingleton.getInstance().setToken(strToken); // keep token
                        }

                        @Override
                        public void failure(RetrofitError error) {
                            error.printStackTrace();
                        }
                    });
                    startActivity(new Intent(getActivity(), MainFragmentActivity.class));
                    Toast.makeText(getContext(), "Update successful", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void failure(RetrofitError error) {
                    Toast.makeText(getContext(), "Update failed", Toast.LENGTH_SHORT).show();
                }
            });

        }
    }
    /**
     * Static function , called to set the selected image
     */
    public static void setImage(Context context) {
//        Bitmap bitmap;
//        // Decode image size
//        BitmapFactory.Options o = new BitmapFactory.Options();
//        o.inJustDecodeBounds = true;
//        BitmapFactory.decodeFile(filePath, o);
//
//        // The new size we want to scale to
//        final int REQUIRED_SIZE = 1024;
//
//        // Find the correct scale value . It should be the power of 2
//        int width_tmp = o.outWidth, height_tmp = o.outHeight;
//
//
//        int scale = 1;
//        while (true) {
//            if (width_tmp < REQUIRED_SIZE && height_tmp < REQUIRED_SIZE)
//                break;
//            width_tmp /= 2;
//            height_tmp /= 2;
//            scale *= 2;
//        }
//
//        // Decode with inSampleSize
//        BitmapFactory.Options o2 = new BitmapFactory.Options();
//        o2.inSampleSize = scale;
//        bitmap = BitmapFactory.decodeFile(filePath, o2);
//
//        imageView.setImageBitmap(bitmap);
        File file = new File(filePath);
        Picasso.with(context)
                .load(file)
                .into(imageView);
    }
    /**
     * Called when fragment is attached with activity
     * @param context - activity context
     */
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            callback = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }
    /**
     * Called when fragment is unlinked from the activity
     */
    @Override
    public void onDetach() {
        super.onDetach();
        callback = null;
    }
    /**
     * Check that all fields are valid
     * @return true if valid
     * false, if not
     */
    public boolean ValidationRegister() {
        String strName = name.getText().toString().trim();
        String namePattern = "^[\\p{L} '-]+$";

        String strEmail = email.getText().toString().trim();
        String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

        String strPassword = password.getText().toString().trim();
        String passwordPattern = ".*\\d+.*";

        String strPassword_confirmation = password_confirmation.getText().toString().trim();


        String strPhone = phone_number.getText().toString().trim();
        String regexStr = "^[\\+[0-9]]{10,13}$";

        String strAddress = address.getText().toString().trim();

        if (strName.isEmpty()) {
            name.requestFocus();
            Drawable myIcon = getResources().getDrawable(R.drawable.icon_invalid);
            myIcon.setBounds(0, 0, myIcon.getIntrinsicWidth(), myIcon.getIntrinsicHeight());
            name.setError("NAME CANNOT BE EMPTY", myIcon);
            return false;
        }
        if (!strName.matches(namePattern)) {
            name.requestFocus();
            Drawable myIcon = getResources().getDrawable(R.drawable.icon_invalid);
            myIcon.setBounds(0, 0, myIcon.getIntrinsicWidth(), myIcon.getIntrinsicHeight());
            name.setError("NAME CANNOT CONTAIN NUMBERS", myIcon);
            return false;
        }
        if (strName.length() < 5 || !strName.contains(" ")) {
            name.requestFocus();
            Drawable myIcon = getResources().getDrawable(R.drawable.icon_invalid);
            myIcon.setBounds(0, 0, myIcon.getIntrinsicWidth(), myIcon.getIntrinsicHeight());
            name.setError("NAME MUST BE: FIRST_NAME LAST NAME");
            return false;
        }
        if (strEmail.isEmpty()) {
            email.requestFocus();
            Drawable myIcon = getResources().getDrawable(R.drawable.icon_invalid);
            myIcon.setBounds(0, 0, myIcon.getIntrinsicWidth(), myIcon.getIntrinsicHeight());
            email.setError("EMAIL CANNOT BE EMPTY", myIcon);
            return false;
        }
        if (!strEmail.matches(emailPattern)) {
            email.requestFocus();
            Drawable myIcon = getResources().getDrawable(R.drawable.icon_invalid);
            myIcon.setBounds(0, 0, myIcon.getIntrinsicWidth(), myIcon.getIntrinsicHeight());
            email.setError("WRONG EMAIL", myIcon);
            return false;
        }
        if (strPassword.isEmpty()) {
            password.requestFocus();
            Drawable myIcon = getResources().getDrawable(R.drawable.icon_invalid);
            myIcon.setBounds(0, 0, myIcon.getIntrinsicWidth(), myIcon.getIntrinsicHeight());
            password.setError("PASSWORD CANNOT BE EMPTY", myIcon);
            return false;
        }
        if (strPassword.length() < 6) {
            password.requestFocus();
            Drawable myIcon = getResources().getDrawable(R.drawable.icon_invalid);
            myIcon.setBounds(0, 0, myIcon.getIntrinsicWidth(), myIcon.getIntrinsicHeight());
            password.setError("Password must have 6 characters ", myIcon);
            return false;
        }
        if (!strPassword.matches(passwordPattern)) {
            Drawable myIcon = getResources().getDrawable(R.drawable.icon_invalid);
            myIcon.setBounds(0, 0, myIcon.getIntrinsicWidth(), myIcon.getIntrinsicHeight());
            password.setError("Password must contain at least one number", myIcon);
            return false;
        }

        if (strPassword_confirmation.isEmpty()) {
            password_confirmation.requestFocus();
            Drawable myIcon = getResources().getDrawable(R.drawable.icon_invalid);
            myIcon.setBounds(0, 0, myIcon.getIntrinsicWidth(), myIcon.getIntrinsicHeight());
            password_confirmation.setError("REPASSWORD CANNOT BE EMPTY", myIcon);
            return false;
        }
        if (!strPassword_confirmation.equals(strPassword)) {
            password_confirmation.requestFocus();
            Drawable myIcon = getResources().getDrawable(R.drawable.icon_invalid);
            myIcon.setBounds(0, 0, myIcon.getIntrinsicWidth(), myIcon.getIntrinsicHeight());
            password_confirmation.setError("Password does not match!", myIcon);
            return false;
        }

        if (strPhone.isEmpty()) {
            phone_number.requestFocus();
            Drawable myIcon = getResources().getDrawable(R.drawable.icon_invalid);
            myIcon.setBounds(0, 0, myIcon.getIntrinsicWidth(), myIcon.getIntrinsicHeight());
            phone_number.setError("PHONE NUMBER CANNOT BE EMPTY", myIcon);
            return false;
        }
        if (!strPhone.matches(regexStr)) {
            phone_number.requestFocus();
            Drawable myIcon = getResources().getDrawable(R.drawable.icon_invalid);
            myIcon.setBounds(0, 0, myIcon.getIntrinsicWidth(), myIcon.getIntrinsicHeight());
            phone_number.setError("INVALID NUMBER : must contains 10 - 13 characters", myIcon);
            return false;
        }
        if (strAddress.isEmpty()) {
            address.requestFocus();
            Drawable myIcon = getResources().getDrawable(R.drawable.icon_invalid);
            myIcon.setBounds(0, 0, myIcon.getIntrinsicWidth(), myIcon.getIntrinsicHeight());
            address.setError("ADDRESS CANNOT BE EMPTY", myIcon);
            return false;
        }
        if (strAddress.length() < 5) {
            address.requestFocus();
            Drawable myIcon = getResources().getDrawable(R.drawable.icon_invalid);
            myIcon.setBounds(0, 0, myIcon.getIntrinsicWidth(), myIcon.getIntrinsicHeight());
            address.setError("ADDRESS MUST CONTAIN AT LEAST 5 CHARACTERS", myIcon);
            return false;
        }

        return true;
    }


    /**
     * Check that all fields are valid
     * @return true if valid
     * false, if not
     */
    public boolean ValidationProfile() {
        String strName = name.getText().toString().trim();
        String namePattern = "^[\\p{L} '-]+$";

        String strPhone = phone_number.getText().toString().trim();
        String regexStr = "^[\\+[0-9]]{10,13}$";

        String strAddress = address.getText().toString().trim();

        if (strName.isEmpty()) {
            name.requestFocus();
            Drawable myIcon = getResources().getDrawable(R.drawable.icon_invalid);
            myIcon.setBounds(0, 0, myIcon.getIntrinsicWidth(), myIcon.getIntrinsicHeight());
            name.setError("NAME CANNOT BE EMPTY", myIcon);
            return false;
        }
        if (!strName.matches(namePattern)) {
            name.requestFocus();
            Drawable myIcon = getResources().getDrawable(R.drawable.icon_invalid);
            myIcon.setBounds(0, 0, myIcon.getIntrinsicWidth(), myIcon.getIntrinsicHeight());
            name.setError("NAME CANNOT CONTAIN NUMBERS", myIcon);
            return false;
        }
        if (strName.length() < 5 || !strName.contains(" ")) {
            name.requestFocus();
            Drawable myIcon = getResources().getDrawable(R.drawable.icon_invalid);
            myIcon.setBounds(0, 0, myIcon.getIntrinsicWidth(), myIcon.getIntrinsicHeight());
            name.setError("NAME MUST BE: FIRST_NAME LAST NAME");
            return false;
        }
        if (strPhone.isEmpty()) {
            phone_number.requestFocus();
            Drawable myIcon = getResources().getDrawable(R.drawable.icon_invalid);
            myIcon.setBounds(0, 0, myIcon.getIntrinsicWidth(), myIcon.getIntrinsicHeight());
            phone_number.setError("PHONE NUMBER CANNOT BE EMPTY", myIcon);
            return false;
        }
        if (!strPhone.matches(regexStr)) {
            phone_number.requestFocus();
            Drawable myIcon = getResources().getDrawable(R.drawable.icon_invalid);
            myIcon.setBounds(0, 0, myIcon.getIntrinsicWidth(), myIcon.getIntrinsicHeight());
            phone_number.setError("INVALID NUMBER : must contains 10 - 13 characters", myIcon);
            return false;
        }
        if (strAddress.isEmpty()) {
            address.requestFocus();
            Drawable myIcon = getResources().getDrawable(R.drawable.icon_invalid);
            myIcon.setBounds(0, 0, myIcon.getIntrinsicWidth(), myIcon.getIntrinsicHeight());
            address.setError("ADDRESS CANNOT BE EMPTY", myIcon);
            return false;
        }
        if (strAddress.length() < 5) {
            address.requestFocus();
            Drawable myIcon = getResources().getDrawable(R.drawable.icon_invalid);
            myIcon.setBounds(0, 0, myIcon.getIntrinsicWidth(), myIcon.getIntrinsicHeight());
            address.setError("ADDRESS MUST CONTAIN AT LEAST 5 CHARACTERS", myIcon);
            return false;
        }

        return true;
    }
    /**
     * Hide password fields
     * Used for edit Profile
     */
    public void settingsForProfileView() {
        sendButton.setText(R.string.sidebar_editProfile);
        email.setEnabled(false);
        textInputPassword.setVisibility(View.INVISIBLE);
        textInputPasswordConfirmation.setVisibility(View.INVISIBLE);
        password.setVisibility(View.GONE);
        password_confirmation.setVisibility(View.GONE);
    }

    /**
     * Fill fields with updated data
     */
    public void fillProfileFieldData() {
        String strName = UserSingleton.getInstance().getUserInformation().getName();
        String strEmail = UserSingleton.getInstance().getUserInformation().getEmail();
        String strPhone = UserSingleton.getInstance().getUserInformation().getPhone();
        String strAddress = UserSingleton.getInstance().getUserInformation().getAddress();
        // fill fields with updated data
        email.setText(strEmail);
        name.setText(strName);
        phone_number.setText(strPhone);
        address.setText(strAddress);
        Picasso.with(getContext())
                .load(filePath)
                .into(image);
    }

    /**
     * Hide image
     * Used for registration Profile
     */
    public void settingsForRegistrationView() {
        sendButton.setText(R.string.register_signUp);
        image.setVisibility(View.GONE);
    }


    /**
     * Call function when register button was clicked
     * Set userInformation with data from fields, after validation
     * Send those data , to server
     * and make login
     */
    private void RegistrationClicked() {
        if (ValidationRegister()) {
            // populate User Information with data
            userInformation.setName(name.getText().toString());
            userInformation.setEmail(email.getText().toString());
            userInformation.setPassword(password.getText().toString());
            userInformation.setPassword_confirmation(password_confirmation.getText().toString());
            userInformation.setPhone(phone_number.getText().toString());
            userInformation.setAddress(address.getText().toString());

            RestClient.networkHandler().sendDataRegister(userInformation, new Callback<JSONObject>() {
                @Override
                public void success(JSONObject message, Response response) {

                    Login();


                }

                @Override
                public void failure(RetrofitError error) {
                    Toast.makeText(getContext(), "ERROR REG " + error, Toast.LENGTH_SHORT).show();
                    error.printStackTrace();
                }
            });
        }
    }

    /**
     * Send data login to server
     * retains and sets the token
     */
    private void Login() {

        userLogin.setEmail(email.getText().toString());
        userLogin.setPassword(password.getText().toString());
        RestClient.networkHandler().sendDataLogin(userLogin, new Callback<LoginResponse>() {


            @Override
            public void success(final LoginResponse loginResponse, Response response) {
                Toast.makeText(getContext(), "SUCCES! ", Toast.LENGTH_SHORT).show();
                UserSingleton.getInstance().setToken(loginResponse.getAuth_token());
                String token = UserSingleton.getInstance().getToken();
                // make callback to get user data
                RestClient.networkHandler().getUserProfile(token, new Callback<UserInformation>() {
                    @Override
                    public void success(UserInformation userInformation, Response response) {
                        UserSingleton.getInstance().setUserInformation(userInformation);
                        Intent intent = new Intent(getContext(), MainFragmentActivity.class);
                        UserSingleton.getInstance().setToken(loginResponse.getAuth_token());
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        Toast.makeText(getContext(), "ERROR GET USER DATA" + error, Toast.LENGTH_SHORT).show();
                        error.printStackTrace();
                    }
                });


            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(getContext(), "ERROR AUTOLOGIN" + error, Toast.LENGTH_SHORT).show();
                error.printStackTrace();
            }


        });
    }

    /**
     * Populate the fields with details about user information
     * received from the server
     */
    public void populateUserFields() {
        name.setText(UserSingleton.getInstance().getUserInformation().getName());
        email.setText(UserSingleton.getInstance().getUserInformation().getEmail());
        password.setText(UserSingleton.getInstance().getUserInformation().getPassword());
        password_confirmation.setText(UserSingleton.getInstance().getUserInformation().getPassword_confirmation());
        phone_number.setText(UserSingleton.getInstance().getUserInformation().getPhone());
        address.setText(UserSingleton.getInstance().getUserInformation().getAddress());
    }

    /**
     * Populate the fields with data taken from Facebook
     */
    public void populateWithFacebookData(String _name, String _email) {
        name.setText(_name);
        email.setText(_email);
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        void onUserDataFragmentInteraction(View view);
    }

}
