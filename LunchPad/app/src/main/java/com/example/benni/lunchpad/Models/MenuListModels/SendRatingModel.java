package com.example.benni.lunchpad.Models.MenuListModels;

/**
 * Created bye BENNI on 9/30/2016.
 *
 */

public class SendRatingModel {
    private Integer rating;
    private String order_id;
    private String token;

    public SendRatingModel(){}

    public SendRatingModel(Integer rating, String order_id, String token) {
        this.rating = rating;
        this.order_id = order_id;
        this.token = token;
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
