package com.example.benni.lunchpad.Tests;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.benni.lunchpad.Models.NewOrderCall;
import com.example.benni.lunchpad.R;
import com.example.benni.lunchpad.Retrofit.RestClient;

import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created bye BENNI on 9/15/2016.
 * Populate list with menus
 */
public class MenuListDetailAdapterOLD extends BaseAdapter {

    private Context mContext; // the context of activity
    private List<MenuOLD> mMenuOLDList; // contains list of menus

    // Constructor
    public MenuListDetailAdapterOLD(Context mContext, List<MenuOLD> mMenuOLDList){
        this.mContext = mContext;
        this.mMenuOLDList = mMenuOLDList;
    }

    // size of list
    @Override
    public int getCount() {
        return mMenuOLDList.size();
    }

    // Get the item from that position
    @Override
    public Object getItem(int position) {
        return mMenuOLDList.get(position);
    }

    // Get the item id from that position
    @Override
    public long getItemId(int position) {
        return position;
    }

    // Take the necessary information and display
    @Override
    public View getView(final int position, View view,final ViewGroup parent) {

        View v = View.inflate(mContext, R.layout.old_list_detail_menus,null);
        TextView textTitle = (TextView)v.findViewById(R.id.item_menu_list_textNameMenu);
        TextView textFirstDish = (TextView)v.findViewById(R.id.item_menu_list_textFirstDish);
        TextView textSecondDish = (TextView)v.findViewById(R.id.item_menu_list_textSecondDish);
        TextView textDessert = (TextView)v.findViewById(R.id.item_menu_list_textDessert);

        textTitle.setText(mMenuOLDList.get(position).getTitle());
        textFirstDish.setText(mMenuOLDList.get(position).getFirstDish());
        textSecondDish.setText(mMenuOLDList.get(position).getSecondDish());
        textDessert.setText(mMenuOLDList.get(position).getDessert());

        Button accept = (Button)v.findViewById(R.id.item_menu_list_buttonOrder);
        accept.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                if (view.getId() == R.id.item_menu_list_buttonOrder) {
                    NewOrderCall newOrderCall = new NewOrderCall();
                    newOrderCall.setMenu_id(mMenuOLDList.get(position).getId());

                    RestClient.networkHandler().SendDataOrderId(newOrderCall.getMenu_id(), new Callback<Menus>() {
                        @Override
                        public void success(Menus menu, Response response) {

                            Toast.makeText(parent.getContext(), "am trimis cu succes ", Toast.LENGTH_SHORT).show();
                          //  Intent detailintent = new Intent(parent.getContext(), OrderView.class);
                         //   parent.getContext().startActivity(detailintent);
                        }

                        @Override
                        public void failure(RetrofitError error) {
                            Toast.makeText(parent.getContext(), "Eroare la order " + error, Toast.LENGTH_SHORT).show();
                            error.printStackTrace();
                        }
                    });
                }
            }
        });

        return v;
    }

}
